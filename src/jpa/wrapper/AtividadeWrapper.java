package jpa.wrapper;

import java.io.Serializable;
import java.util.Date;

public class AtividadeWrapper implements Serializable{

	private static final long serialVersionUID = 2076293895577795637L;
	
	private String nomeAtividade;
	private double valorTotalAtividade;
	private double notaDoAluno;
	private String codAtividade;
	private String codDisciplina;
	private String codTurma;
	private int anoTurma;
	private long areaAtividade;
	private Date dataAbertura;
	private Date dataEncerramento;
	private Date dataAlunoComecou;
	private Date dataAlunoTerminou;
	private int finalizada;
	
	public AtividadeWrapper(){
		
	}

	public AtividadeWrapper(String nomeAtividade, double valorTotalAtividade,
			double notaDoAluno, String codAtividade, String codDisciplina,
			String codTurma, int anoTurma, long areaAtividade,
			Date dataAbertura, Date dataEncerramento, Date dataAlunoComecou,
			Date dataAlunoTerminou, int finalizada) {
		super();
		this.nomeAtividade = nomeAtividade;
		this.valorTotalAtividade = valorTotalAtividade;
		this.notaDoAluno = notaDoAluno;
		this.codAtividade = codAtividade;
		this.codDisciplina = codDisciplina;
		this.codTurma = codTurma;
		this.anoTurma = anoTurma;
		this.areaAtividade = areaAtividade;
		this.dataAbertura = dataAbertura;
		this.dataEncerramento = dataEncerramento;
		this.dataAlunoComecou = dataAlunoComecou;
		this.dataAlunoTerminou = dataAlunoTerminou;
		this.finalizada = finalizada;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anoTurma;
		result = prime * result
				+ (int) (areaAtividade ^ (areaAtividade >>> 32));
		result = prime * result
				+ ((codAtividade == null) ? 0 : codAtividade.hashCode());
		result = prime * result
				+ ((codDisciplina == null) ? 0 : codDisciplina.hashCode());
		result = prime * result
				+ ((codTurma == null) ? 0 : codTurma.hashCode());
		result = prime * result
				+ ((dataAbertura == null) ? 0 : dataAbertura.hashCode());
		result = prime
				* result
				+ ((dataAlunoComecou == null) ? 0 : dataAlunoComecou.hashCode());
		result = prime
				* result
				+ ((dataAlunoTerminou == null) ? 0 : dataAlunoTerminou
						.hashCode());
		result = prime
				* result
				+ ((dataEncerramento == null) ? 0 : dataEncerramento.hashCode());
		result = prime * result + finalizada;
		result = prime * result
				+ ((nomeAtividade == null) ? 0 : nomeAtividade.hashCode());
		long temp;
		temp = Double.doubleToLongBits(notaDoAluno);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(valorTotalAtividade);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeWrapper other = (AtividadeWrapper) obj;
		if (anoTurma != other.anoTurma)
			return false;
		if (areaAtividade != other.areaAtividade)
			return false;
		if (codAtividade == null) {
			if (other.codAtividade != null)
				return false;
		} else if (!codAtividade.equals(other.codAtividade))
			return false;
		if (codDisciplina == null) {
			if (other.codDisciplina != null)
				return false;
		} else if (!codDisciplina.equals(other.codDisciplina))
			return false;
		if (codTurma == null) {
			if (other.codTurma != null)
				return false;
		} else if (!codTurma.equals(other.codTurma))
			return false;
		if (dataAbertura == null) {
			if (other.dataAbertura != null)
				return false;
		} else if (!dataAbertura.equals(other.dataAbertura))
			return false;
		if (dataAlunoComecou == null) {
			if (other.dataAlunoComecou != null)
				return false;
		} else if (!dataAlunoComecou.equals(other.dataAlunoComecou))
			return false;
		if (dataAlunoTerminou == null) {
			if (other.dataAlunoTerminou != null)
				return false;
		} else if (!dataAlunoTerminou.equals(other.dataAlunoTerminou))
			return false;
		if (dataEncerramento == null) {
			if (other.dataEncerramento != null)
				return false;
		} else if (!dataEncerramento.equals(other.dataEncerramento))
			return false;
		if (finalizada != other.finalizada)
			return false;
		if (nomeAtividade == null) {
			if (other.nomeAtividade != null)
				return false;
		} else if (!nomeAtividade.equals(other.nomeAtividade))
			return false;
		if (Double.doubleToLongBits(notaDoAluno) != Double
				.doubleToLongBits(other.notaDoAluno))
			return false;
		if (Double.doubleToLongBits(valorTotalAtividade) != Double
				.doubleToLongBits(other.valorTotalAtividade))
			return false;
		return true;
	}

	public String getNomeAtividade() {
		return nomeAtividade;
	}

	public void setNomeAtividade(String nomeAtividade) {
		this.nomeAtividade = nomeAtividade;
	}

	public double getValorTotalAtividade() {
		return valorTotalAtividade;
	}

	public void setValorTotalAtividade(double valorTotalAtividade) {
		this.valorTotalAtividade = valorTotalAtividade;
	}

	public double getNotaDoAluno() {
		return notaDoAluno;
	}

	public void setNotaDoAluno(double notaDoAluno) {
		this.notaDoAluno = notaDoAluno;
	}

	public String getCodAtividade() {
		return codAtividade;
	}

	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public int getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}

	public long getAreaAtividade() {
		return areaAtividade;
	}

	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public Date getDataAlunoComecou() {
		return dataAlunoComecou;
	}

	public void setDataAlunoComecou(Date dataAlunoComecou) {
		this.dataAlunoComecou = dataAlunoComecou;
	}

	public Date getDataAlunoTerminou() {
		return dataAlunoTerminou;
	}

	public void setDataAlunoTerminou(Date dataAlunoTerminou) {
		this.dataAlunoTerminou = dataAlunoTerminou;
	}

	public int getFinalizada() {
		return finalizada;
	}

	public void setFinalizada(int finalizada) {
		this.finalizada = finalizada;
	}
	
	
	
}
