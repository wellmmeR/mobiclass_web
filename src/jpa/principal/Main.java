package jpa.principal;

import javax.xml.ws.Endpoint;

import jpa.mgr.academico.MgrAcademico;
import jpa.mgr.atividade.MgrAtividade;

public class Main {

	public static void main(String[] args){		
		
		
		
		Thread academico = new Thread(new Runnable() {
			@Override
			public void run() {
				MgrAcademico m = new MgrAcademico();
				Endpoint.publish("http://localhost:8081/academico", m);
				
			}
		});
		Thread atividade = new Thread(new Runnable() {
			
			@Override
			public void run() {
				MgrAtividade a = new MgrAtividade();
				Endpoint.publish("http://localhost:8081/atividade", a);
			}
		});
		
		academico.run();
		atividade.run();
	}
}