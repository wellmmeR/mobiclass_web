package jpa.pessoa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PROFESSOR", schema = "mobiclass")
public class Professor extends Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	public Professor() {

	}

	public Professor(long RA, String nome, String dataNasc, String email,
			String senha, String sexo) {
		setRA(RA);
		setNome(nome);
		setDataNasc(dataNasc);
		setEmail(email);
		setSenha(senha);
		setLogado(0);
		setSexo(sexo);
	}

	public String toString() {
		return "||" + getRA() + '|' + getNome() + '|' + getDataNasc() + '|'
				+ getEmail() + "||";
	}
}
