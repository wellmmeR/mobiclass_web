package jpa.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

public class Where {
	public static final String WHERE = "WHERE";
	
	private List<Condicao> condicoesValidas;
	private String stringWhere;
	
	private Where() {
		condicoesValidas = new ArrayList<Condicao>();
	}
	public static Where criarWhere(List<Condicao> lista){
		
		Where where = new Where();
		StringBuilder stringWhere = new StringBuilder();
		for(Condicao c : lista){
			if(validarTipo(c)){
				stringWhere.append(WHERE);
				break;
			}
		}
		if(stringWhere.length() == 0){
			where.setStringWhere("");
			return  where;
		}
		arrumarAsCondicoesLike(lista);	
		int index = 1;
		int indexParametro = 1;
		for(Condicao c : lista){
			if(validarTipo(c)){
				if(index > 1){
					stringWhere.append(" AND");
				}
				stringWhere.append(" ");
				stringWhere.append(c.getAtributo()); 
				stringWhere.append(c.getOperador().getValor());
				if(!c.isLiteral()){
					stringWhere.append("?" + indexParametro++);
					where.condicoesValidas.add(new Condicao(c.getAtributo(), c.getValue(), c.getOperador()));
				}	
				else
					stringWhere.append(c.getValue());
				index++;
			}
		}
		where.setStringWhere(stringWhere.toString());		
		return where;
	}
	
	private static void arrumarAsCondicoesLike(List<Condicao> lista){
		for(Condicao c : lista){
			if(CondicaoEnum.LIKE.equals(c.getOperador()))
				if(validarTipo(c) && c.getValue() instanceof String)
					c.setValue("%" + c.getValue().toString() + "%");
		}
	}
	
	private static boolean validarTipo(Condicao c){
		Object valor = c.getValue();
		if(valor == null)
			return false;
		if(valor instanceof Long && valor.equals(Desconsiderar.tipoLong))
			return false;
		if(valor instanceof String && valor.equals(Desconsiderar.tipoString))
			 	return false;
		if(valor instanceof Integer && valor.equals(Desconsiderar.tipoInt))
		 	return false;
		if(valor instanceof Double && valor.equals(Desconsiderar.tipoDouble))
			return false;
		return true;
	}
	
	public void setParametros(TypedQuery<?> query, List<Condicao> lista){
		if(query == null || lista == null)
			return;
		
		int index =1;
		
		for(Condicao condicao : lista){
			query.setParameter(index, condicao.getValue());
			index++;
		}
	}

	public List<Condicao> getCondicoesValidas() {
		return condicoesValidas;
	}

	public void setCondicoesValidas(List<Condicao> condicoesValidas) {
		this.condicoesValidas = condicoesValidas;
	}

	public String getStringWhere() {
		return stringWhere;
	}

	public void setStringWhere(String stringWhere) {
		this.stringWhere = stringWhere;
	}

}
