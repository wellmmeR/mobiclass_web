package jpa.atividade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AREA_ATIVIDADE", schema = "mobiclass")
public class AreaAtividade implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long id;
	@Column(name = "AREA_ATIVIDADE")
	private String areaAtividade;

	public AreaAtividade() {

	}

	public AreaAtividade(String AreaAtividade) {
		this.setAreaAtividade(AreaAtividade);
	}

	public String toString() {
		return "||" + getId() + '|' + getAreaAtividade() + "||";
	}

	public String getAreaAtividade() {
		return areaAtividade;
	}

	public void setAreaAtividade(String areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
