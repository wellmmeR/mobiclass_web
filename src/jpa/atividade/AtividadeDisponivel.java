package jpa.atividade;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ATIVIDADE_DISPONIVEL", schema = "mobiclass")
public class AtividadeDisponivel implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AtividadeDisponivelID id;

	@Column(name = "DAT_LIBERACAO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataLiberacao;

	@Column(name = "DAT_ENCERRAMENTO", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEncerramento;

	@Column(name = "FLG_DISPONIVEL", nullable = false)
	private int isDisponivel;

	@Column(name = "VAL_ATIVIDADE", nullable = true, scale = 2, precision = 2)
	private double valorAtividade;

	public double getValorAtividade() {
		return valorAtividade;
	}

	public void setValorAtividade(double valorAtividade) {
		this.valorAtividade = valorAtividade;
	}

	public AtividadeDisponivel(String codAtv, String codDis, String codTurma, int anoTurma, String datL, String datE,
			int isDisponivel, double valorAtividade, long areaAtividade) {

		setId(new AtividadeDisponivelID(codAtv, codDis, codTurma, anoTurma, areaAtividade));
		setDataLiberacao(datL);
		setDataEncerramento(datE);
		setIsDisponivel(isDisponivel);
		setValorAtividade(valorAtividade);
	}

	public AtividadeDisponivel(String codAtv, String codDis, String codTurma, int anoTurma, Date dataLiberacao,
			Date dataEncerramento, int isDisponivel, long areaAtividade, double valorAtividade) {
		setId(new AtividadeDisponivelID(codAtv, codDis, codTurma, anoTurma, areaAtividade));
		this.dataLiberacao = dataLiberacao;
		this.dataEncerramento = dataEncerramento;
		this.isDisponivel = isDisponivel;
		this.valorAtividade = valorAtividade;
	}

	public int getIsDisponivel() {
		return isDisponivel;
	}

	public void setIsDisponivel(int isDisponivel) {
		this.isDisponivel = isDisponivel;
	}

	public Date getDataLiberacao() {
		return dataLiberacao;
	}

	public void setDataLiberacao(String dataLiberacao) {
		this.dataLiberacao = converteData(dataLiberacao);
	}

	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(String dataEncerramento) {
		this.dataEncerramento = converteData(dataEncerramento);
	}

	public AtividadeDisponivelID getId() {
		return id;
	}

	public void setId(AtividadeDisponivelID id) {
		this.id = id;
	}

	public String toString() {
		return "||" + getId().getCodAtividade() + '|' + getId().getAreaAtividade() + '|' + getId().getCodDisciplina()
				+ '|' + getId().getCodTurma() + '|' + getId().getAnoTurma() + '|' + getDataLiberacao() + '|'
				+ getDataEncerramento() + "||";
	}

	public AtividadeDisponivel() {
		setId(new AtividadeDisponivelID());
	}

	private Date converteData(String data) {
		Date date = null;
		try {
			if (data != null) {
				SimpleDateFormat dtOutput = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				date = dtOutput.parse(data);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataEncerramento == null) ? 0 : dataEncerramento.hashCode());
		result = prime * result + ((dataLiberacao == null) ? 0 : dataLiberacao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + isDisponivel;
		long temp;
		temp = Double.doubleToLongBits(valorAtividade);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeDisponivel other = (AtividadeDisponivel) obj;
		if (dataEncerramento == null) {
			if (other.dataEncerramento != null)
				return false;
		} else if (!dataEncerramento.equals(other.dataEncerramento))
			return false;
		if (dataLiberacao == null) {
			if (other.dataLiberacao != null)
				return false;
		} else if (!dataLiberacao.equals(other.dataLiberacao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isDisponivel != other.isDisponivel)
			return false;
		if (Double.doubleToLongBits(valorAtividade) != Double.doubleToLongBits(other.valorAtividade))
			return false;
		return true;
	}

}
