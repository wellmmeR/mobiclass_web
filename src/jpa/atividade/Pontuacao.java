package jpa.atividade;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ALUNO_PONTOS", schema = "mobiclass")
public class Pontuacao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private PontuacaoID id;
	
	@Column(name="DATA", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataTarefa;
	@Column(name="DESCRICAO", nullable = true)
	private String descricao;
	@Column(name="PONTOS", nullable = false)
	private double pontos;
	@Column(name="COD_LANCAMENTO", nullable = false)
	private String codLancamento;
		
	
	public String getCodLancamento() {
		return codLancamento;
	}
	public void setCodLancamento(String codLancamento) {
		this.codLancamento = codLancamento;
	}

	public Date getDataTarefa() {
		return dataTarefa;
	}

	public void setDataTarefa(Date dataTarefa) {
		this.dataTarefa = dataTarefa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getPontos() {
		return pontos;
	}

	public void setPontos(double pontos) {
		this.pontos = pontos;
	}
	
	public PontuacaoID getId() {
		return id;
	}
	public void setId(PontuacaoID id) {
		this.id = id;
	}
	
	public String toString(){
		return"||"+
		id.getRaAluno()+'|'+
		id.getCodAtividade()+'|'+
		id.getAreaAtividade()+'|'+
		id.getCodDisciplina()+'|'+
		getDataTarefa()+'|'+
		getDescricao()+'|'+
		getPontos()+"||"; 
	}
	
	public Pontuacao() {
		setId(new PontuacaoID());
	}
	
	
	public Pontuacao(long ra, String codAtividade, String codDisciplina, String codTurma, 
		int anoTurma, String descricao, double pontos, String codLancamento, long areaAtividade){
		Date hj = new Date();
		SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		setId(new PontuacaoID(ra, codAtividade, codDisciplina, anoTurma, codTurma, areaAtividade));
		
		setDataTarefa(converteData(formatData.format(hj)));
		setDescricao(descricao);
		setPontos(pontos);
		setCodLancamento(codLancamento);
	
	}
	private Date converteData(String data) {  
	    Date date = null;  
	    try {  
	        SimpleDateFormat dtOutput = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	        date = dtOutput.parse(data);
	    }
	    catch (ParseException e) {  
	        e.printStackTrace();  
	    }  
	    return date;  
	}
}
