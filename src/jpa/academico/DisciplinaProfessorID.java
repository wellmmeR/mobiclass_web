package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DisciplinaProfessorID implements Serializable{

	private static final long serialVersionUID = 8469594697416177175L;
	@Column(name="COD_DISCIPLINA", nullable = false)
	private String codDisciplina;
	@Column(name="COD_PROFESSOR", nullable = false)
	private long raProfessor;
	
	public DisciplinaProfessorID() {
	}

	public DisciplinaProfessorID(String codDisciplina, long raProfessor) {
		this.setCodDisciplina(codDisciplina);
		this.setRaProfessor(raProfessor);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getCodDisciplina() == null) ? 0 : getCodDisciplina().hashCode());
		result = prime * result + (int) (getRaProfessor() ^ (getRaProfessor() >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisciplinaProfessorID other = (DisciplinaProfessorID) obj;
		if (getCodDisciplina() == null) {
			if (other.getCodDisciplina() != null)
				return false;
		} else if (!getCodDisciplina().equals(other.getCodDisciplina()))
			return false;
		if (getRaProfessor() != other.getRaProfessor())
			return false;
		return true;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public long getRaProfessor() {
		return raProfessor;
	}

	public void setRaProfessor(long raProfessor) {
		this.raProfessor = raProfessor;
	}
	
	
	
	
}
