package jpa.academico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AlunoDisciplinaTurmaID implements Serializable{

	private static final long serialVersionUID = -6127207300009706093L;
	@Column(name = "COD_DISCIPLINA", nullable = false)
	private String codDisciplina;
	@Column(name = "COD_TURMA", nullable = false)
	private String codTurma;
	@Column(name = "ANO_TURMA", nullable = false)
	private int anoTurma;
	@Column(name = "COD_ALUNO", nullable = false)
	private long RA;
	
	public AlunoDisciplinaTurmaID() {
	}
	
	public AlunoDisciplinaTurmaID(String codDisciplina, String codTurma, int anoTurma, long RA) {
		this.codDisciplina = codDisciplina;
		this.codTurma = codTurma;
		this.anoTurma = anoTurma;
		this.RA = RA;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (RA ^ (RA >>> 32));
		result = prime * result + anoTurma;
		result = prime * result
				+ ((codDisciplina == null) ? 0 : codDisciplina.hashCode());
		result = prime * result
				+ ((codTurma == null) ? 0 : codTurma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoDisciplinaTurmaID other = (AlunoDisciplinaTurmaID) obj;
		if (RA != other.RA)
			return false;
		if (anoTurma != other.anoTurma)
			return false;
		if (codDisciplina == null) {
			if (other.codDisciplina != null)
				return false;
		} else if (!codDisciplina.equals(other.codDisciplina))
			return false;
		if (codTurma == null) {
			if (other.codTurma != null)
				return false;
		} else if (!codTurma.equals(other.codTurma))
			return false;
		return true;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public int getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}

	public long getRA() {
		return RA;
	}

	public void setRA(long rA) {
		RA = rA;
	}
	
	
}
