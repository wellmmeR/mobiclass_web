package jpa.academico;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="PROFESSOR_DISCIPLINA", schema = "mobiclass")
public class DisciplinaProfessor implements Serializable{
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private DisciplinaProfessorID id;
	
	public DisciplinaProfessor() {
		new DisciplinaProfessorID();
	}
	
	public DisciplinaProfessor(long raProfessor, String codDisciplina) {
		setId(new DisciplinaProfessorID(codDisciplina, raProfessor));
	}	
	
	public String toString(){
		return "||"+id.getCodDisciplina()+"|"+id.getRaProfessor()+"||";
	}

	public DisciplinaProfessorID getId() {
		return id;
	}

	public void setId(DisciplinaProfessorID id) {
		this.id = id;
	}
	
}
