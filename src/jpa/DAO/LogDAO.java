package jpa.DAO;

import jpa.constante.log.Log;

public class LogDAO extends DAO<Log, Long>{

	private static final long serialVersionUID = 608662234015564896L;
	@Override
	public Class<Log> getType() {
		return Log.class;
	}

}
