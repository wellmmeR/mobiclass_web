package jpa.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.relacionamento.AlunoDisciplinaTurmaDAO;
import jpa.DAO.relacionamento.DisciplinaProfessorDAO;
import jpa.DAO.relacionamento.TurmaDisciplinaDAO;
import jpa.academico.Disciplina;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class DisciplinaDAO extends DAO<Disciplina, String> {

	private static final long serialVersionUID = 1L;

	@Override
	public Class<Disciplina> getType() {
		return Disciplina.class;
	}
	
	@Override
	public void deletarRelacionamentos(String id) throws Exception {
		AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
		DisciplinaProfessorDAO dpDao = new DisciplinaProfessorDAO();
		TurmaDisciplinaDAO tdDao = new TurmaDisciplinaDAO();
		AtividadeDAO aDao = new AtividadeDAO();
		
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		Map<String, Object> subMapa= new HashMap<>();
		
		subMapa.put("codDisciplina", id);
		mapaDeAtributos.put("id", subMapa);
		adtDao.deletarPorId(adtDao.buscarPorAtributos(mapaDeAtributos));
		dpDao.deletarPorId(dpDao.buscarPorAtributos(mapaDeAtributos));
		tdDao.deletarPorId(tdDao.buscarPorAtributos(mapaDeAtributos));
		
		mapaDeAtributos.clear();
		subMapa.clear();
		subMapa.put("codigoDisciplina", id);
		mapaDeAtributos.put("idAtividade", subMapa);
		aDao.deletarPorId(aDao.buscarPorAtributos(mapaDeAtributos));
		
	}
	
	@Override
	public void deletarRelacionamentos(List<Disciplina> lista) throws Exception {
		for(Disciplina d:lista){
			deletarPorId(d.getCodigoDisciplina());
		}
	}
	
	public List<Disciplina> buscarDisciplinas(Disciplina d) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("d.codigoDisciplina", d.getCodigoDisciplina(), CondicaoEnum.LIKE));
		condicoes.add(new Condicao("d.nome", d.getNome(), CondicaoEnum.LIKE));
		Where w = Where.criarWhere(condicoes);
		
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT d FROM Disciplina d " + w.getStringWhere();
		TypedQuery<Disciplina> query= em.createQuery(jpql, Disciplina.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Disciplina> lista = query.getResultList();
		em.close();
		return lista;
	}

}
