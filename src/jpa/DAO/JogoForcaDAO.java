package jpa.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.atividade.JogoForca;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class JogoForcaDAO extends DAO<JogoForca, Long>{

	private static final long serialVersionUID = -8437364650745297806L;

	@Override
	public Class<JogoForca> getType() {
		return JogoForca.class;
	}
	
	public List<JogoForca> buscarJogosForca(JogoForca jgForca) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("jg.areaAtividade", jgForca.getAreaAtividade()));
		condicoes.add(new Condicao("jg.codAtividade", jgForca.getCodAtividade()));
		condicoes.add(new Condicao("jg.codDisciplina", jgForca.getCodDisciplina()));
		condicoes.add(new Condicao("jg.codJogo", jgForca.getCodJogo()));
		condicoes.add(new Condicao("jg.dica", jgForca.getDica(), CondicaoEnum.LIKE));
		condicoes.add(new Condicao("jg.disponivel", jgForca.getDisponivel()));
		condicoes.add(new Condicao("jg.numTentativas", jgForca.getNumTentativas()));
		condicoes.add(new Condicao("jg.palavra", jgForca.getPalavra(), CondicaoEnum.LIKE));
		Where w = Where.criarWhere(condicoes);
		String jqpl = "SELECT jg FROM JogoForca jg " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<JogoForca> query = em.createQuery(jqpl, JogoForca.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<JogoForca> jogosForca = query.getResultList();
		em.close();
		return jogosForca;
	}

}
