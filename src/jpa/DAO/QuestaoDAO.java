package jpa.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.atividade.Questao;
import jpa.util.Condicao;
import jpa.util.Where;

public class QuestaoDAO extends DAO<Questao, Long>{

	private static final long serialVersionUID = 6691938784414756209L;

	@Override
	public Class<Questao> getType() {
		return Questao.class;
	}
	
	@Override
	public void deletarRelacionamentos(List<Questao> lista) throws Exception {
		for(Questao q : lista){
			deletarRelacionamentos(q.getCodPergunta());
		}
	}
	
	@Override
	public void deletarRelacionamentos(Long id) throws Exception {
		AlternativaDAO aDao = new AlternativaDAO();
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		mapaDeAtributos.put("codPergunta", id);
		aDao.deletarPorId(aDao.buscarPorAtributos(mapaDeAtributos));
	}
	
	public List<Questao> buscarQuestao(Questao q) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("q.codPergunta",q.getCodPergunta()));
		condicoes.add(new Condicao("q.nomeQuestao",q.getNomeQuestao()));
		condicoes.add(new Condicao("q.codDisciplina",q.getCodDisciplina()));
		condicoes.add(new Condicao("q.isDisponivel",q.getIsDisponivel()));
		condicoes.add(new Condicao("q.valorPergunta",q.getValorPergunta()));
		condicoes.add(new Condicao("q.categoria",q.getCategoria()));
		condicoes.add(new Condicao("q.codAtividade",q.getCodAtividade()));
		condicoes.add(new Condicao("q.areaAtividade",q.getAreaAtividade()));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT q FROM Questao q " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<Questao> query = em.createQuery(jpql,Questao.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Questao> lista = query.getResultList();
		em.close();
		return lista;
	}

}
