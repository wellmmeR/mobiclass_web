package jpa.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.atividade.Pontuacao;
import jpa.atividade.PontuacaoID;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class PontuacaoDAO extends DAO<Pontuacao, PontuacaoID>{

	private static final long serialVersionUID = -1397127996584554493L;

	@Override
	public Class<Pontuacao> getType() {
		return Pontuacao.class;
	}
	
	public List<Pontuacao> buscarPontuacao(Pontuacao p) throws Exception{
		
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("p.id.raAluno",p.getId().getRaAluno()));
		condicoes.add(new Condicao("p.id.codAtividade",p.getId().getCodAtividade()));
		condicoes.add(new Condicao("p.id.codDisciplina",p.getId().getCodDisciplina()));
		condicoes.add(new Condicao("p.id.anoTurma",p.getId().getAnoTurma()));
		condicoes.add(new Condicao("p.id.codTurma",p.getId().getCodTurma()));
		condicoes.add(new Condicao("p.id.areaAtividade",p.getId().getAreaAtividade()));
		condicoes.add(new Condicao("p.descricao",p.getDescricao()));
		condicoes.add(new Condicao("p.pontos",p.getPontos()));
		condicoes.add(new Condicao("p.codLancamento",p.getCodLancamento(), CondicaoEnum.LIKE));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT p FROM Pontuacao p " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<Pontuacao> query = em.createQuery(jpql,Pontuacao.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Pontuacao> lista = query.getResultList();
		em.close();
		return lista;
	}

}
