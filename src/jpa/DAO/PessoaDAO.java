package jpa.DAO;

import jpa.pessoa.Pessoa;

public class PessoaDAO extends DAO<Pessoa, Long> {

	private static final long serialVersionUID = 1L;

	@Override
	public Class<Pessoa> getType() {
		return Pessoa.class;
	}
	
	

}
