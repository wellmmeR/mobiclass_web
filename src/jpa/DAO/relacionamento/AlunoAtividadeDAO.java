package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.DAO.PontuacaoDAO;
import jpa.atividade.AtividadeExecutada;
import jpa.atividade.AtividadeExecutadaID;
import jpa.util.Condicao;
import jpa.util.Where;

public class AlunoAtividadeDAO extends DAO<AtividadeExecutada, AtividadeExecutadaID>{

	private static final long serialVersionUID = -4482750245870375349L;

	@Override
	public Class<AtividadeExecutada> getType() {
		return AtividadeExecutada.class;
	}
	
	@Override
	public void deletarRelacionamentos(AtividadeExecutadaID id) throws Exception{
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		Map<String, Object> subMapa= new HashMap<>();
		
		PontuacaoDAO pDao = new PontuacaoDAO();
		
		subMapa.put("raAluno", id.getCodAluno());
		subMapa.put("anoTurma", id.getAnoTurma());
		subMapa.put("areaAtividade", id.getAreaAtividade());
		subMapa.put("codAtividade", id.getCodAtividade());
		subMapa.put("codDisciplina", id.getCodDisciplina());
		subMapa.put("codTurma", id.getCodTurma());
		mapaDeAtributos.put("id", subMapa);
		pDao.deletarPorId(pDao.buscarPorAtributos(mapaDeAtributos));
	}
	
	@Override
	public void deletarRelacionamentos(List<AtividadeExecutada> lista)
			throws Exception {
		for(AtividadeExecutada aEx : lista){
			deletarRelacionamentos(aEx.getId());
		}
	}
	
	public List<AtividadeExecutada> buscarAtividadesExecutadas(AtividadeExecutada aExec) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("aExec.id.codAluno", aExec.getId().getCodAluno()));
		condicoes.add(new Condicao("aExec.id.codTurma", aExec.getId().getCodTurma()));
		condicoes.add(new Condicao("aExec.id.anoTurma", aExec.getId().getAnoTurma()));
		condicoes.add(new Condicao("aExec.id.codAtividade", aExec.getId().getCodAtividade()));
		condicoes.add(new Condicao("aExec.id.codDisciplina", aExec.getId().getCodDisciplina()));
		condicoes.add(new Condicao("aExec.isFinalizado", aExec.getIsFinalizado()));
		condicoes.add(new Condicao("aExec.id.areaAtividade", aExec.getId().getAreaAtividade()));
		condicoes.add(new Condicao("aExec.isFinalizado", aExec.getIsFinalizado()));
		

		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT aExec FROM AtividadeExecutada aExec " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<AtividadeExecutada> query = em.createQuery(jpql, AtividadeExecutada.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<AtividadeExecutada> lista = query.getResultList();
		em.close();
		return lista;
	}

}
