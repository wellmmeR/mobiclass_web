package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.academico.ProfessorTurma;
import jpa.academico.ProfessorTurmaID;
import jpa.academico.Turma;
import jpa.util.Condicao;
import jpa.util.Where;

public class ProfessorTurmaDAO extends DAO<ProfessorTurma, ProfessorTurmaID>{

	private static final long serialVersionUID = -5449773870943942230L;

	@Override
	public Class<ProfessorTurma> getType() {
		return ProfessorTurma.class;
	}
	
	public List<Turma> buscarTurmasDoProfessor(Long raProfessor, Turma t) throws Exception{
		
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("pf.id.codTurma", "t.id.codigoTurma", true));
		condicoes.add(new Condicao("pf.id.anoTurma", "t.id.anoTurma", true));
		condicoes.add(new Condicao("pf.id.codProfessor", raProfessor));
		condicoes.add(new Condicao("t.id.anoTurma", t.getId().getAnoTurma()));
		Where w = Where.criarWhere(condicoes);
		EntityManager em = factory.createEntityManager();
		String jpql = "Select t FROM Turma t, ProfessorTurma pf " + w.getStringWhere();
		TypedQuery<Turma> query= em.createQuery(jpql, Turma.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Turma> turmas = query.getResultList();
		em.close();
		return turmas;
	}

}
