package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.academico.Disciplina;
import jpa.academico.DisciplinaProfessor;
import jpa.academico.DisciplinaProfessorID;
import jpa.util.Condicao;
import jpa.util.Where;

public class DisciplinaProfessorDAO extends DAO<DisciplinaProfessor, DisciplinaProfessorID>{

	private static final long serialVersionUID = -4393045403764965634L;

	@Override
	public Class<DisciplinaProfessor> getType() {
		return DisciplinaProfessor.class;
	}
	
	public List<Disciplina> buscarDisciplinasDoProfessor(Long raProfessor, Disciplina d) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("dp.id.codDisciplina", "d.codigoDisciplina", true));
		condicoes.add(new Condicao("dp.id.raProfessor", raProfessor));
		Where w = Where.criarWhere(condicoes);
		
		EntityManager em = factory.createEntityManager();
		String jpql = "SELECT d FROM Disciplina, DisciplinaProfessor dp " + w.getStringWhere();
		TypedQuery<Disciplina> query = em.createQuery(jpql, Disciplina.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Disciplina> lista = query.getResultList();
		em.close();
		return lista;
	}
	
	
}
