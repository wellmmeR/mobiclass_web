package jpa.DAO.relacionamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.DAO;
import jpa.academico.AlunoCurso;
import jpa.academico.AlunoCursoID;
import jpa.academico.Curso;
import jpa.pessoa.Aluno;
import jpa.pessoa.Pessoa;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class AlunoCursoDAO extends DAO<AlunoCurso, AlunoCursoID>{
	
	private static final long serialVersionUID = 197027799823420014L;

	@Override
	public Class<AlunoCurso> getType() {
		return AlunoCurso.class;
	}
	
	public List<Aluno> buscarAlunosPorCurso(String codCurso, Aluno a) throws Exception{
		try{
			List<Condicao> condicoes = new ArrayList<Condicao>();
			condicoes.add(new Condicao("ac.id.codCurso", codCurso));
			condicoes.add(new Condicao("ac.id.raAluno", "a.RA", true));
			condicoes.add(new Condicao("a.RA", a.getRA()));
			condicoes.add(new Condicao("a.dataNasc", a.getDataNasc()));
			condicoes.add(new Condicao("a.email", a.getEmail(), CondicaoEnum.LIKE));
			condicoes.add(new Condicao("a.nome", a.getNome(), CondicaoEnum.LIKE));
			condicoes.add(new Condicao("a.sexo", a.getSexo(), CondicaoEnum.LIKE));
	
			Where w = Where.criarWhere(condicoes);
			
			String jpql = "select a from Aluno a, AlunoCurso ac " + w.getStringWhere();
			EntityManager em = factory.createEntityManager();
			TypedQuery<Aluno> query = em.createQuery(jpql, Aluno.class);
			w.setParametros(query, w.getCondicoesValidas());
			List<Aluno> lista = query.getResultList();
			em.close();
			return lista;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
	}
	
	public List<Curso> buscarCursosPorAluno(Curso curso, Long raAluno) throws Exception{
		try{
			List<Condicao> condicoes = new ArrayList<Condicao>();
			condicoes.add(new Condicao("ac.id.codCurso", "c.codCurso", true));
			condicoes.add(new Condicao("ac.id.raAluno", raAluno));
			condicoes.add(new Condicao("c.codCurso", curso.getCodCurso()));
			condicoes.add(new Condicao("c.nome", curso.getNome(), CondicaoEnum.LIKE));
			condicoes.add(new Condicao("c.isDisponivel", curso.getIsDisponivel()));
	
			Where w = Where.criarWhere(condicoes);
			
			String jpql = "select c from Curso c, AlunoCurso ac " + w.getStringWhere();
			EntityManager em = factory.createEntityManager();
			TypedQuery<Curso> query = em.createQuery(jpql, Curso.class);
			w.setParametros(query, w.getCondicoesValidas());
			List<Curso> lista = query.getResultList();
			em.close();
			return lista;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
	}
	
	public List<Aluno> alunosSemCurso(Pessoa aluno) throws Exception{
		try{
			String stringWhere = "";
			Where w;
			List<Condicao> condicoes = new ArrayList<>();
			if(aluno != null)
			{
				condicoes.add(new Condicao("a.RA", aluno.getRA()));
				condicoes.add(new Condicao("a.dataNasc", aluno.getDataNasc()));
				condicoes.add(new Condicao("a.email", aluno.getEmail(), CondicaoEnum.LIKE));
				condicoes.add(new Condicao("a.nome", aluno.getNome(), CondicaoEnum.LIKE));
				condicoes.add(new Condicao("a.sexo", aluno.getSexo(), CondicaoEnum.LIKE));
			}
			w = Where.criarWhere(condicoes);
			stringWhere = w.getStringWhere();
			String condicaoPrincipal= "a.RA NOT IN (select ac.id.raAluno from AlunoCurso ac)";
			if(stringWhere.isEmpty())
				condicaoPrincipal = " WHERE " + condicaoPrincipal; 
			else
				condicaoPrincipal = " " + stringWhere + " AND " + condicaoPrincipal;
			
			EntityManager em = factory.createEntityManager();	
			String jpql = "SELECT a FROM Aluno a" + condicaoPrincipal;
			TypedQuery<Aluno> query = em.createQuery(jpql, Aluno.class);
			w.setParametros(query, w.getCondicoesValidas());
			List<Aluno> alunosSemCurso = query.getResultList();
			em.close();
			return alunosSemCurso;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw ex;
		}
	}
	

}
