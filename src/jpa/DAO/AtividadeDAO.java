package jpa.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.relacionamento.AtividadeDisponivelDAO;
import jpa.atividade.Atividade;
import jpa.atividade.AtividadeID;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class AtividadeDAO extends DAO<Atividade, AtividadeID> {

	private static final long serialVersionUID = 1L;

	@Override
	public Class<Atividade> getType() {
		return Atividade.class;
	}
	
	@Override
	public void deletarRelacionamentos(AtividadeID id) throws Exception {
		PaginaSlideDAO psDao = new PaginaSlideDAO();
		QuestaoDAO qDao = new QuestaoDAO();
		JogoForcaDAO jgDao = new JogoForcaDAO();
		AtividadeDisponivelDAO adDao = new AtividadeDisponivelDAO();
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		Map<String, Object> subMapa= new HashMap<>();
		
		mapaDeAtributos.put("codAtividade", id.getCodigoAtividade());
		mapaDeAtributos.put("codDisciplina", id.getCodigoDisciplina());
		mapaDeAtributos.put("areaAtividade", id.getAreaAtividade());
		psDao.deletarPorId(psDao.buscarPorAtributos(mapaDeAtributos));
		qDao.deletarPorId(qDao.buscarPorAtributos(mapaDeAtributos));
		jgDao.deletarPorId(jgDao.buscarPorAtributos(mapaDeAtributos));
		
		mapaDeAtributos.clear();
		subMapa.put("codAtividade", id.getCodigoAtividade());
		subMapa.put("codDisciplina", id.getCodigoDisciplina());
		subMapa.put("areaAtividade", id.getAreaAtividade());
		mapaDeAtributos.put("id", subMapa);
		adDao.deletarPorId(adDao.buscarPorAtributos(mapaDeAtributos));
		
	}
	
	@Override
	public void deletarRelacionamentos(List<Atividade> lista) throws Exception {
		for(Atividade atividade : lista){
			deletarRelacionamentos(atividade.getIdAtividade());
		}
	}
	
	public List<Atividade> buscarAtividade(Atividade a) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("a.idAtividade.codigoAtividade", a.getIdAtividade().getCodigoAtividade()));
		condicoes.add(new Condicao("a.idAtividade.codigoDisciplina", a.getIdAtividade().getCodigoDisciplina()));
		condicoes.add(new Condicao("a.idAtividade.areaAtividade", a.getIdAtividade().getAreaAtividade()));
		condicoes.add(new Condicao("a.nome", a.getNome(), CondicaoEnum.LIKE));
		condicoes.add(new Condicao("a.disponivel", a.isDisponivel()));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT a FROM Atividade a " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<Atividade> query = em.createQuery(jpql, Atividade.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Atividade> lista = query.getResultList();
		em.close();
		return lista;
	}
	
	public Atividade buscarSingleAtividade(Atividade a) throws Exception{
		List<Atividade> atividades = buscarAtividade(a);
		if(atividades == null || atividades.isEmpty())
			return null;
		return atividades.get(0);
	}
	

}
