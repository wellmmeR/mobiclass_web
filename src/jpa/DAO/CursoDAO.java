package jpa.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.relacionamento.AlunoCursoDAO;
import jpa.DAO.relacionamento.ProfessorTurmaDAO;
import jpa.academico.Curso;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class CursoDAO extends DAO<Curso, String>{

	private static final long serialVersionUID = 1L;

	@Override
	public Class<Curso>getType() {
		return Curso.class;
	}
	
	@Override
	public void deletarRelacionamentos(String id) throws Exception{
		AlunoCursoDAO acDao = new AlunoCursoDAO();
		TurmaDAO tDao = new TurmaDAO();
		ProfessorTurmaDAO ptDao = new ProfessorTurmaDAO();
		
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		Map<String, Object> subMapa= new HashMap<>();
		
		subMapa.put("codCurso", id);
		mapaDeAtributos.put("id", subMapa);
		acDao.deletarPorId(acDao.buscarPorAtributos(mapaDeAtributos));
		mapaDeAtributos.clear();
		subMapa.clear();
		
		mapaDeAtributos.put("codCurso", id);
		tDao.deletarPorId(tDao.buscarPorAtributos(mapaDeAtributos));
		ptDao.deletarPorId(ptDao.buscarPorAtributos(mapaDeAtributos));
		mapaDeAtributos.clear();
	}
	
	public List<Curso> buscarTodosCursosDisponiveis() throws Exception{
		try{
			EntityManager em = factory.createEntityManager();
			String jpql = "SELECT c from Curso c WHERE c.isDisponivel=:disp";
			TypedQuery<Curso> query = em.createQuery(jpql, Curso.class);
			query.setParameter("disp", 1);
			return query.getResultList();
		}
		catch(Exception ex){
			throw ex;
		}
	}
	
	public List<Curso> buscarCursos(String codigo, String nome, Integer disponivel) throws Exception{
		try{
			List<Condicao> condicoes = new ArrayList<Condicao>();
			condicoes.add(new Condicao("codCurso", codigo));
			condicoes.add(new Condicao("nome", nome, CondicaoEnum.LIKE));
			condicoes.add(new Condicao("isDisponivel",disponivel));
			Where w = Where.criarWhere(condicoes);
			EntityManager em = factory.createEntityManager();
			String jpql = "SELECT c FROM Curso c " + w.getStringWhere();
			TypedQuery<Curso> query = em.createQuery(jpql, Curso.class);
			w.setParametros(query, w.getCondicoesValidas());
			List<Curso> cursos = query.getResultList();
			em.close();
			return cursos;
		}
		catch(Exception ex){
			throw ex;
		}
	}

}
