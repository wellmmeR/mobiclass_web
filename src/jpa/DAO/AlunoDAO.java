package jpa.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.DAO.relacionamento.AlunoAtividadeDAO;
import jpa.DAO.relacionamento.AlunoCursoDAO;
import jpa.DAO.relacionamento.AlunoDisciplinaTurmaDAO;
import jpa.DAO.relacionamento.AlunoTurmaDAO;
import jpa.academico.Curso;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.atividade.AtividadeExecutada;
import jpa.pessoa.Aluno;
import jpa.util.Condicao;
import jpa.util.CondicaoEnum;
import jpa.util.Where;

public class AlunoDAO extends DAO<Aluno, Long>{
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public Class<Aluno> getType() {
		return Aluno.class;
	}
	
	@Override
	public void deletarRelacionamentos(Long id) throws Exception{

		AlunoCursoDAO acDao = new AlunoCursoDAO();
		AlunoTurmaDAO atDao = new AlunoTurmaDAO();
		
		AlunoDisciplinaTurmaDAO adtDao = new AlunoDisciplinaTurmaDAO();
		AlunoAtividadeDAO aaDao = new AlunoAtividadeDAO();
		
		Map<String, Object> mapaDeAtributos = new HashMap<>();
		Map<String, Object> subMapa= new HashMap<>();
		
		subMapa.put("raAluno", id);
		mapaDeAtributos.put("id", subMapa);
		acDao.deletarPorId(acDao.buscarPorAtributos(mapaDeAtributos));
		mapaDeAtributos.clear();
		subMapa.clear();
		
		subMapa.put("RA", id);
		mapaDeAtributos.put("id", subMapa);
		atDao.deletarPorId(atDao.buscarPorAtributos(mapaDeAtributos));
		mapaDeAtributos.clear();
		subMapa.clear();
		
		subMapa.put("RA", id);
		mapaDeAtributos.put("id", subMapa);
		adtDao.deletarPorId(adtDao.buscarPorAtributos(mapaDeAtributos));
		mapaDeAtributos.clear();
		subMapa.clear();
		
		subMapa.put("codAluno",id);
		mapaDeAtributos.put("id", subMapa);
		aaDao.deletarPorId(aaDao.buscarPorAtributos(mapaDeAtributos));
		mapaDeAtributos.clear();
		subMapa.clear();
		
	}
	
	@Override
	public void deletarRelacionamentos(List<Aluno> lista)throws Exception{
		for(Aluno aluno: lista){
			deletarRelacionamentos(aluno.getRA());
		}
	}
	public List<AtividadeExecutada> consultarTodasAtividadesExecutadasDoAluno(Long id){
		String jpql = "select ate from "
				+ "Aluno a, AtividadeExecutada ate "
				+ "where a.RA = ate.codAluno and a.RA = :ra";
		TypedQuery<AtividadeExecutada> q = factory.createEntityManager().createQuery(jpql,AtividadeExecutada.class);
		q.setParameter("ra", id);
		return q.getResultList();
	}
	
	public List<Disciplina> consultarTodasDisciplinasDoAluno(Long id){
		String jpql = "select d from "
				+ "Aluno a, AlunoDisciplinaTurma, Disciplina d "
				+ "where a.RA = s.RA and s.codDisciplina = d.codigoDisciplina and a.RA = :ra";
		TypedQuery<Disciplina> q = factory.createEntityManager().createQuery(jpql, Disciplina.class);
		q.setParameter("ra", id);
		return q.getResultList();
	}

	public List<Curso> consultarTodosCursosDoAluno(Long id){
		String jpql = "select c from "
				+ "Aluno a, AlunoCurso ac, Curso c "
				+ "where a.RA = ac.id.raAluno and ac.id.codCurso = c.codCurso and a.RA = :ra";
		TypedQuery<Curso> q = factory.createEntityManager().createQuery(jpql,Curso.class);
		q.setParameter("ra", id);
		return q.getResultList();
	}

	public List<Turma> consultarTodasTurmasDoAluno(Long id){
		String jpql = "select t from "
				+ "Aluno a, AlunoTurma s, Turma t "
				+ "where a.RA = s.id.RA and s.id.codTurma = t.id.codigoTurma and a.RA = :ra";
		TypedQuery<Turma> q = factory.createEntityManager().createQuery(jpql, Turma.class);
		q.setParameter("ra", id);
		return q.getResultList();
	}
	
	public long primeiroRADisponivel(){
		String jpql = "select a.RA from Aluno a";
		TypedQuery<Long> q = factory.createEntityManager().createQuery(jpql, Long.class);
		List<Long> codigos = q.getResultList();
		if(codigos.size()>0)
		{
			long raDisponivel = 1;
			for(Long ra : codigos){
				if(ra > raDisponivel)
					break;
				raDisponivel++;
			}
			return raDisponivel;
		}
		else
			return 1L;
	}
	
	public List<Long> primeirosRADisponivel(int quantidade){
		String jpql = "select a.RA from Aluno a";
		TypedQuery<Long> q = factory.createEntityManager().createQuery(jpql, Long.class);
		SortedSet<Long> codigos = new TreeSet<>(q.getResultList());
		List<Long> codigosDisponiveis = new ArrayList<Long>();
		
		long raDisponivel = 1;
		while(codigosDisponiveis.size() < quantidade)
		{
			if(!codigos.contains(raDisponivel))
			{
				codigosDisponiveis.add(raDisponivel);
			}
			raDisponivel++;
		}	
		return codigosDisponiveis;
	}
	
	public List<Aluno> buscarAlunos(Aluno aluno) throws Exception{
		String stringWhere = "";
		Where w;
		List<Condicao> condicoes = new ArrayList<>();
		if(aluno != null)
		{
			condicoes.add(new Condicao("a.RA", aluno.getRA()));
			condicoes.add(new Condicao("a.dataNasc", aluno.getDataNasc()));
			condicoes.add(new Condicao("a.email", aluno.getEmail(), CondicaoEnum.LIKE));
			condicoes.add(new Condicao("a.nome", aluno.getNome(), CondicaoEnum.LIKE));
			condicoes.add(new Condicao("a.sexo", aluno.getSexo(), CondicaoEnum.LIKE));
		}
		w = Where.criarWhere(condicoes);
		stringWhere = w.getStringWhere();
		
		
		EntityManager em = factory.createEntityManager();	
		String jpql = "SELECT a FROM Aluno a " + stringWhere;
		TypedQuery<Aluno> query = em.createQuery(jpql, Aluno.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<Aluno> alunos = query.getResultList();
		em.close();
		return alunos;
	}
}

