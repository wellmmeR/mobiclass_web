package jpa.DAO;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.TypedQuery;

import jpa.pessoa.Professor;

public class ProfessorDAO extends DAO<Professor, Long> {

	private static final long serialVersionUID = 1L;

	@Override
	public Class<Professor> getType() {
		return Professor.class;
	}
	
	public long primeiroRADisponivel() throws Exception{
		String jpql = "select p.RA from Professor p";
		TypedQuery<Long> q = factory.createEntityManager().createQuery(jpql, Long.class);
		
		List<Long> codigos = q.getResultList();
		if(codigos.size()>0)
		{
			long raDisponivel = 1;
			for(Long ra : codigos){
				if(ra > raDisponivel)
					break;
				raDisponivel++;
			}
			return raDisponivel;
		}
		else
			return 1L;
	}
	
	public List<Long> primeirosRADisponivel(int quantidade){
		String jpql = "select p.RA from Professor p";
		TypedQuery<Long> q = factory.createEntityManager().createQuery(jpql, Long.class);
		SortedSet<Long> codigos = new TreeSet<>(q.getResultList());
		List<Long> codigosDisponiveis = new ArrayList<Long>();
		
		long raDisponivel = 1;
		while(codigosDisponiveis.size() < quantidade)
		{
			if(!codigos.contains(raDisponivel))
			{
				codigosDisponiveis.add(raDisponivel);
			}
			raDisponivel++;
		}	
		return codigosDisponiveis;
	}

}
