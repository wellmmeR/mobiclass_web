package jpa.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import jpa.atividade.PaginaSlide;
import jpa.util.Condicao;
import jpa.util.Where;

public class PaginaSlideDAO extends DAO<PaginaSlide, Long>{

	private static final long serialVersionUID = -6920779359900721773L;

	@Override
	public Class<PaginaSlide> getType() {
		return PaginaSlide.class;
	}
	
	public List<PaginaSlide> buscarPaginaSlide(PaginaSlide ps) throws Exception{
		List<Condicao> condicoes = new ArrayList<Condicao>();
		condicoes.add(new Condicao("ps.id", ps.getId()));
		condicoes.add(new Condicao("ps.codAtividade", ps.getCodAtividade()));
		condicoes.add(new Condicao("ps.codTurma", ps.getCodTurma()));
		condicoes.add(new Condicao("ps.codDisciplina", ps.getCodDisciplina()));
		condicoes.add(new Condicao("ps.anoTurma", ps.getAnoTurma()));
		condicoes.add(new Condicao("ps.data", ps.getData()));
		condicoes.add(new Condicao("ps.totalPagina", ps.getTotalPagina()));
		condicoes.add(new Condicao("ps.paginaAtual", ps.getPaginaAtual()));
		condicoes.add(new Condicao("ps.areaAtividade", ps.getAreaAtividade()));
		Where w = Where.criarWhere(condicoes);
		String jpql = "SELECT ps FROM PaginaSlide " + w.getStringWhere();
		EntityManager em = factory.createEntityManager();
		TypedQuery<PaginaSlide> query = em.createQuery(jpql, PaginaSlide.class);
		w.setParametros(query, w.getCondicoesValidas());
		List<PaginaSlide> lista = query.getResultList();
		em.close();
		return lista;
	}
	
	public PaginaSlide buscarSinglePaginaSlide(PaginaSlide ps) throws Exception{
		List<PaginaSlide> lista = buscarPaginaSlide(ps);
		if(lista == null || lista.size() == 0)
			return null;
		return lista.get(0);
	}

}
