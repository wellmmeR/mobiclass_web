package jpa.testes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import jpa.DAO.AlunoDAO;
import jpa.academico.Curso;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.atividade.AtividadeExecutada;
import jpa.pessoa.Aluno;

import org.junit.Assert;
import org.junit.Test;

public class AlunoDAOTester {

	@Test
	public void testGetType() {
		AlunoDAO dao = new AlunoDAO();
		assertEquals(dao.getType(), Aluno.class);
		
	}

	@Test
	public void testConsultarTodasAtividadesExecutadasDoAluno() {
		AlunoDAO dao = new AlunoDAO();
		List<AtividadeExecutada> lista = dao.consultarTodasAtividadesExecutadasDoAluno(1L);
		if(lista == null || lista.size()==0)
		{
			fail("Lista de Atividades igual a 0");
		}
		
		
	}

	@Test
	public void testConsultarTodasDisciplinasDoAluno() {
		AlunoDAO dao = new AlunoDAO();
		List<Disciplina> lista = dao.consultarTodasDisciplinasDoAluno(1L);
		if(lista == null || lista.size()==0)
		{
			fail("Lista de Disciplinas igual a 0");
		}
		
		
	}

	@Test
	public void testConsultarTodosCursosDoAluno() {
		AlunoDAO dao = new AlunoDAO();
		List<Curso> lista = dao.consultarTodosCursosDoAluno(1L);
		if(lista == null || lista.size()==0)
		{
			fail("Lista de Cursos igual a 0");
		}
		
		
	}

	@Test
	public void testConsultarTodasTurmasDoAluno() {
		AlunoDAO dao = new AlunoDAO();
		List<Turma> lista = dao.consultarTodasTurmasDoAluno(1L);
		if(lista == null || lista.size()==0)
		{
			fail("Lista de Turmas igual a 0");
		}
		
		
	}

	@Test
	public void testPrimeiroRADisponivel() {
		AlunoDAO dao = new AlunoDAO();
		Long primeiroRA = dao.primeiroRADisponivel();
		
		Assert.assertTrue(primeiroRA!=null);
	}

	@Test
	public void testAdicionar() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		long RA = dao.primeiroRADisponivel();
		Aluno a = new Aluno(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
		dao.adicionar(a);
		if(dao.consultarPorId(RA) == null)
			fail("aluno criado n�o foi encontrado no banco");
		
		dao.deletarPorId(RA);
	}
	
	@Test
	public void testAdicionarListOfAluno() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		
		List<Aluno> alunos = new ArrayList<Aluno>();
		List<Long> codigos = new ArrayList<Long>();
		for(int i=0;i<4;i++){
			long RA = dao.primeiroRADisponivel()+i;
			Aluno a = new Aluno(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
			codigos.add(RA);
			alunos.add(a);
			
		}
		dao.adicionar(alunos);
		
		for(Long codigo : codigos){
			if(dao.consultarPorId(codigo)==null)
				fail("Aluno n�o foi inserido");
		}	
		for(Long codigo: codigos){
			dao.deletarPorId(codigo);
		}
		
	}

	@Test
	public void testAtualizar() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		long RA = dao.primeiroRADisponivel();
		String nome = "Teste"; 
		String dataNascimento = "09/04/1992";
		String email = "teste@hotmail.com";
		String senha = "123";
		String sexo = "M";
		Aluno a = new Aluno(RA, nome, dataNascimento, email, senha, sexo);
		dao.adicionar(a);
		
		a = new Aluno(RA, "Testee", "10/04/1992","testee@hotmail.com", "1234", "F"); 
		dao.atualizar(a);
		a = dao.consultarPorId(RA);
		if(a.getNome().equals(nome) || a.getDataNasc().equals(dataNascimento) || a.getEmail().equals(email) || a.getSenha().equals(senha) || a.getSexo().equals(sexo))
			fail("Alguma coisa n�o mudou");
		dao.deletarPorId(RA);
	}
	
	@Test
	public void testAtualizarListOfAlunos() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		
		List<Aluno> alunos = new ArrayList<Aluno>();
		List<Long> codigos = new ArrayList<Long>();
		for(int i=0;i<4;i++){
			long RA = dao.primeiroRADisponivel() + i;
			String nome = "Teste"; 
			String dataNascimento = "09/04/1992";
			String email = "teste@hotmail.com";
			String senha = "123";
			String sexo = "M";
			
			alunos.add(new Aluno(RA, nome, dataNascimento, email, senha, sexo));
			codigos.add(RA);
		}
		dao.adicionar(alunos);
		List<Aluno> alunos2 = new ArrayList<Aluno>();
		
		for(Long codigo: codigos){
			Aluno a = dao.consultarPorId(codigo);
			a.setNome("Testee");
			a.setDataNasc("10/04/1992");
			a.setEmail("testee@hotmail.com");
			a.setSenha("1234");
			a.setSexo("F");
			alunos2.add(a);
		}
		dao.atualizar(alunos2);
		
		alunos2 = new ArrayList<Aluno>();
		
		for(Long codigo : codigos){
			alunos2.add(dao.consultarPorId(codigo));
		}
		
		for(int i=0;i<4;i++){
			Aluno a1 = alunos.get(i);
			Aluno a2 = alunos2.get(i);
			
			if(a1.getNome().equals(a2.getNome()) || a1.getDataNasc().equals(a2.getDataNasc()) || a1.getEmail().equals(a2.getEmail()) || a1.getSenha().equals(a2.getSenha()) || a1.getSexo().equals(a2.getSexo()))
				fail("Alguma coisa n�o mudou");
		}
		for(Long codigo : codigos){
			dao.deletarPorId(codigo);
		}
		
	}

	@Test
	public void testConsultarPorId() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		
		long RA = dao.primeiroRADisponivel();
		Aluno a = new Aluno(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
		dao.adicionar(a);
		a = dao.consultarPorId(RA);
		if(a == null)
			fail("Consulta n�o trouxe o aluno esperado");
		
		dao.deletarPorId(RA);
		
		
		
	}

	@Test
	public void testDeletarPorIdID() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		long RA = dao.primeiroRADisponivel();
		Aluno a = new Aluno(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
		dao.adicionar(a);
		dao.deletarPorId(RA);
		if(dao.consultarPorId(RA) != null)
			fail("Aluno n�o foi deletado");
			
	}
	
	
	@Test
	public void testDeletarPorIdListOfID() throws Exception{
		AlunoDAO dao = new AlunoDAO();
		List<Aluno> alunos = new ArrayList<Aluno>();
		
		for(int i=0; i<10;i++){
			long RA = dao.primeiroRADisponivel();
			Aluno a = new Aluno(RA, "Teste", "09/04/1992", "teste@hotmail.com", "123", "M");
			dao.adicionar(a);
			alunos.add(a);
		}
		
		dao.deletarPorId(alunos);
		
		for(Aluno aluno:alunos){
			if(dao.consultarPorId(aluno.getRA())!=null)
				fail("Aluno n�o foi deletado");
		}
		
	}

}
