package jpa.loggin;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import jpa.DAO.AlunoDAO;
import jpa.DAO.LogDAO;
import jpa.DAO.ProfessorDAO;
import jpa.constante.log.DadosLog;
import jpa.constante.log.Log;
import jpa.pessoa.Aluno;
import jpa.pessoa.Professor;

public class MgrLoggin {
	private DadosLog lblLog = new DadosLog();
	private final String PERSISTENCE_UNIT_NAME = "ServerAndroid";
	private EntityManagerFactory factory =  Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	//Login----------------------------------------------------------------------
	
	public boolean loginAlunoAtividade(long raAluno, String senha, String codAtividade){		
		try{
			AlunoDAO dao = new AlunoDAO();
			Aluno aluno = dao.consultarPorId(raAluno);
			if (aluno != null && aluno.getSenha().equals(senha)){
				aluno.setLogado(1);
				dao.atualizar(aluno);
				gravaLog(lblLog._alunoLogin+raAluno, null, lblLog._tipoCadastro);
				return true;
			}
			else
			{
				gravaLog(lblLog._erroQuery, "Login Aluno", lblLog._tipoQuery);
				return false;
			}
		}catch(Exception e){
			gravaLog(lblLog._erroLogin+raAluno, null, lblLog._tipoExcecao);
			return false;
		}
	}
	
	public String loginAluno(long raAluno, String senha){		
		try{
			AlunoDAO dao = new AlunoDAO();
			Aluno aluno = dao.consultarPorId(raAluno);
			if (aluno == null){
				gravaLog(lblLog._alunoLogin+raAluno, "RA inexistente", lblLog._tipoCadastro);
				return lblLog._false+"RA inexistente";
			}
			else if(!aluno.getSenha().equals(senha)){
				gravaLog(lblLog._erroQuery, "Login Aluno", lblLog._tipoQuery);
				return lblLog._false+"Senha incorreta";
			}
			else{
				aluno.setLogado(1);
				dao.atualizar(aluno);
				gravaLog(lblLog._alunoLogin+raAluno, null, lblLog._tipoCadastro);
				return lblLog._true+"-Completo";
			}
		}catch(Exception e){
			gravaLog(lblLog._erroLogin+raAluno, null, lblLog._tipoExcecao);
			return lblLog._false+e.getMessage();
		}
	}
	public boolean loginProfessor(long raProfessor, String senha){
		try{
			Professor professor = null;
			ProfessorDAO dao = new ProfessorDAO();
			professor = dao.consultarPorId(raProfessor);
			if (professor != null && professor.getSenha().equals(senha)){
				professor.setLogado(1);
				dao.atualizar(professor);
				gravaLog(lblLog._professorLogin+raProfessor,null, lblLog._tipoCadastro);
				return true;
			}
			else
			{
				gravaLog(lblLog._erroQuery, "Login Professor", lblLog._tipoQuery);			
				return false;
			}
		}catch(Exception e){
			gravaLog(lblLog._erroLogin+raProfessor, null, lblLog._tipoExcecao);
			return false;
		}
	}	
	public int isAlunoLogado(long raAluno, String senhaAluno){
		try{
			Aluno aluno = null;
			AlunoDAO dao = new AlunoDAO(); 
			aluno = dao.consultarPorId(raAluno);
			if(aluno.getSenha().equals(senhaAluno))
				return aluno.getLogado();
			return 0;
		}catch(Exception e){
			return 0;
		}
		
	}
	public int isProfessorLogado(long raProfessor, String senhaProfessor){
		try{
			Professor professor = new ProfessorDAO().consultarPorId(raProfessor);
			if(professor.getSenha().equals(senhaProfessor))
				return professor.getLogado();
			return 0;
		}catch(Exception e){
			return 0;
		}
	}
	public boolean alunoFinalizaSessao(long raAluno, String senhaAluno){
		try{
			Aluno aluno = null;
			AlunoDAO dao = new AlunoDAO();
			aluno = dao.consultarPorId(raAluno);
			if (aluno == null){ 
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return false;
			}
			aluno.setLogado(0);
			dao.atualizar(aluno);
			gravaLog(lblLog._alunoLogout+raAluno, null, lblLog._tipoCadastro);
			return true;
		}catch(Exception e){
			gravaLog("Erro logout: "+raAluno, null, lblLog._tipoExcecao);
			return false;
		}
	}
	public boolean professorFinalizaSessao(long raProfessor, String senhaProfessor){
		try{
			Professor professor = null;
			ProfessorDAO dao = new ProfessorDAO();
			professor = dao.consultarPorId(raProfessor);
			if (professor == null) {
				gravaLog(lblLog._erroQueryVazia, null, lblLog._tipoQuery);
				return false;
			}
			professor.setLogado(0);
			dao.atualizar(professor);
			gravaLog(lblLog._alunoLogout+raProfessor, null, lblLog._tipoCadastro);
			return true;
		}catch(Exception e){
			gravaLog("Erro logout: "+raProfessor, null, lblLog._tipoExcecao);
			return false;
		}
	}
	private void gravaLog(String dadosLog, String detalhe, int tipo){
		try{
			Log log = new Log(dadosLog, detalhe, tipo);
			new LogDAO().adicionar(log);
		}catch(Exception e){
			return;
		}
	}
	
	public void finalizar(){
		if(this.factory != null)
			this.factory.close();
	}
}
