package web.src.controller;

import jpa.DAO.ProfessorDAO;
import jpa.pessoa.Pessoa;
import web.src.entity.NegocioException;
import web.src.entity.Sessao;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;


public class LoginController extends Controller{
	
	private static final long serialVersionUID = 1L;

	public void tentarLogar(String codUsuario, String senha, Sessao sessao) throws NegocioException
	{
		try{
			if(Util.soTemNumeros(codUsuario))
			{
				long cod = Long.parseLong(codUsuario);
				Integer tipo_usuario;
				ProfessorDAO pDao = new ProfessorDAO();
				Pessoa usuario = null;
				
				if((usuario = pDao.consultarPorId(cod)) != null)
					tipo_usuario = Util.TIPO_PROFESSOR;
				else
					throw new NegocioException("RA inexistente");
				sessao.setUsuario(usuario);
				logar(usuario, tipo_usuario, senha);
			}
			else
				throw new NegocioException("RA Inv�lido");
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
		
	}
	private void logar(Pessoa usuario, Integer tipo, String senha) throws NegocioException
	{
		boolean temUsuarioLogado = Util.pegarObjetoDaSessao(SessaoEnum.KEY_USUARIO) != null? true:false;
		if(temUsuarioLogado)
		{
			Util.removerObjetoDaSessao(SessaoEnum.KEY_USUARIO);
			Util.removerObjetoDaSessao(SessaoEnum.KEY_TIPO_USUARIO);
		}
		
		 if(senha.equals(usuario.getSenha())){
			 Util.adicionarObjetoNaSessao(SessaoEnum.KEY_USUARIO, usuario);
			 Util.adicionarObjetoNaSessao(SessaoEnum.KEY_TIPO_USUARIO, tipo);
			 Util.redirecionarParaIndex();
		 }
		 else
		 throw new NegocioException("Senha incorreta");
	}
	
	public void logout(){
		Util.redirecionar("login.xhtml");;
		Util.getHttpSession(true).invalidate();
	}

}
