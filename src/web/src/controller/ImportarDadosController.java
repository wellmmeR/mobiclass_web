package web.src.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jpa.DAO.AlunoDAO;
import jpa.DAO.CursoDAO;
import jpa.DAO.DisciplinaDAO;
import jpa.DAO.ProfessorDAO;
import jpa.DAO.TurmaDAO;
import jpa.academico.Curso;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.pessoa.Aluno;
import jpa.pessoa.Pessoa;
import jpa.pessoa.Professor;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.model.UploadedFile;

import web.src.entity.NegocioException;
public class ImportarDadosController extends Controller{
	

	private static final long serialVersionUID = -1436810815131002630L;
	public static final int IMPORTAR_PESSOA = 0;
	public static final int IMPORTAR_CURSO = 1;
	public static final int IMPORTAR_TURMA = 2;
	public static final int IMPORTAR_DISCIPLINA = 3;
	private static final String REGEX_ANO = "[12][0-9]{3}";
	
	public void importar(UploadedFile file, Integer tipo) throws NegocioException{
		validarArquivoETipo(file, tipo);
		
		if(IMPORTAR_PESSOA == tipo)
			importarPessoas(file);
		else if(IMPORTAR_CURSO == tipo)
			importarCurso(file);
		else if(IMPORTAR_TURMA == tipo)
			importarTurma(file);
		else if(IMPORTAR_DISCIPLINA == tipo)
			importarDisciplina(file);
	}
	
	private void importarCurso(UploadedFile file) throws NegocioException{
		int linhaAtual = 0;
		int colunaAtual = 0;
		List<Curso> cursos = new ArrayList<Curso>();
		try{
			Workbook workbook = WorkbookFactory.create(file.getInputstream());
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowInterator = sheet.rowIterator();
			if(rowInterator.hasNext())
				rowInterator.next();
			while(rowInterator.hasNext()){
				Row row = rowInterator.next();
				Iterator<Cell> cellIterator = row.iterator();
				Curso c = new Curso();
				if(row.getLastCellNum() != 3)
					throw new NegocioException("Quantidade de colunas invalidas");
				while(cellIterator.hasNext()){
					Cell celula = cellIterator.next();
					linhaAtual = celula.getRowIndex();
					colunaAtual = celula.getColumnIndex();
					switch(celula.getColumnIndex()){
						case 0:
							c.setCodCurso(extrairValorCelula(celula));
							break;
						case 1:
							c.setNome(extrairValorCelula(celula));
							break;
						case 2:
							String disponivel = extrairValorCelula(celula);
							if(!disponivel.equals("1") && !disponivel.equals("0"))
								throw new NegocioException(disponivel + " n�o � um tipo valido");
							c.setIsDisponivel(Integer.parseInt(disponivel));
							break;
						default:
							throw new Exception("Quantidade de colunas Invalidas");
					}
				}
				cursos.add(c);
			}
			workbook.close();
		}
		catch(InvalidFormatException iFE){
			throw new NegocioException("Formata��o do arquivo � invalida");
		}
		catch(IllegalStateException iSE){
			throw new NegocioException("Formata��o incorreta na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(NumberFormatException nFE){
			throw new NegocioException("Erro ao tentar converter numero na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(Exception ex){
			throw new NegocioException("Erro na linha " + ++linhaAtual + " coluna " + ++colunaAtual + ": " + ex.getMessage());
		}
		salvarCursos(cursos);
	}
	private void salvarCursos(List<Curso> cursos) throws NegocioException{
		try{
			CursoDAO dao = new CursoDAO();
			for(Curso c: cursos)
			{
				if(dao.consultarPorId(c.getCodCurso()) != null)
					throw new NegocioException(c.getCodCurso() + " j� esta cadastrado");
				dao.adicionar(c);
			}
		}
		catch(Exception nEx){
			throw new NegocioException(nEx.getMessage());
		}
	}

	private void importarTurma(UploadedFile file) throws NegocioException{
		int linhaAtual = 0;
		int colunaAtual = 0;
		List<Turma> turmas = new ArrayList<Turma>();
		try{
			Workbook workbook = WorkbookFactory.create(file.getInputstream());
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowInterator = sheet.rowIterator();
			if(rowInterator.hasNext())
				rowInterator.next();
			while(rowInterator.hasNext()){
				Row row = rowInterator.next();
				Iterator<Cell> cellIterator = row.iterator();
				Turma t = new Turma();
				while(cellIterator.hasNext()){
					Cell celula = cellIterator.next();
					linhaAtual = celula.getRowIndex();
					colunaAtual = celula.getColumnIndex();
					if(row.getLastCellNum() != 4)
						throw new NegocioException("Quantidade de colunas invalidas");
					switch(celula.getColumnIndex()){
						case 0:
							t.getId().setCodigoTurma(extrairValorCelula(celula));
							break;
						case 1:
							String ano = extrairValorCelula(celula);
							if(!ano.matches(REGEX_ANO))
								throw new NegocioException(ano + " n�o � um ano valido");
							t.getId().setAnoTurma(Integer.parseInt(ano));
							break;
						case 2:
							t.setNomeTurma(extrairValorCelula(celula));
							break;
						case 3:
							t.setCodCurso(extrairValorCelula(celula));
							break;
						default:
							throw new Exception("Quantidade de colunas Invalidas");
					}
				}
				turmas.add(t);
			}
			workbook.close();
		}
		catch(InvalidFormatException iFE){
			throw new NegocioException("Formata��o do arquivo � invalida");
		}
		catch(IllegalStateException iSE){
			throw new NegocioException("Formata��o incorreta na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(NumberFormatException nFE){
			throw new NegocioException("Erro ao tentar converter numero na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(Exception ex){
			throw new NegocioException("Erro na linha " + ++linhaAtual + " coluna " + ++colunaAtual + ": " + ex.getMessage());
		}
		
		salvarTurmas(turmas);
	}
	private void salvarTurmas(List<Turma> turmas) throws NegocioException{
		try{
			TurmaDAO dao = new TurmaDAO();
			for(Turma t: turmas)
			{
				if(dao.consultarPorId(t.getId()) != null)
					throw new NegocioException("Turma " + t.getId().getCodigoTurma() + " no ano" + t.getId().getAnoTurma() + " j� esta cadastrada");
				dao.adicionar(t);
			}
		}
		catch(Exception nEx){
			throw new NegocioException(nEx.getMessage());
		}
		
	}

	private void importarDisciplina(UploadedFile file) throws NegocioException{
		try{
			int linhaAtual = 0;
			int colunaAtual = 0;
			List<Disciplina> disciplinas = new ArrayList<Disciplina>();
			try{
				Workbook workbook = WorkbookFactory.create(file.getInputstream());
				Sheet sheet = workbook.getSheetAt(0);
				Iterator<Row> rowInterator = sheet.rowIterator();
				if(rowInterator.hasNext())
					rowInterator.next();
				while(rowInterator.hasNext()){
					Row row = rowInterator.next();
					Iterator<Cell> cellIterator = row.iterator();
					Disciplina d = new Disciplina();
					if(row.getLastCellNum() != 2)
						throw new NegocioException("Quantidade de colunas invalidas");
					while(cellIterator.hasNext()){
						Cell celula = cellIterator.next();
						linhaAtual = celula.getRowIndex();
						colunaAtual = celula.getColumnIndex();
						switch(celula.getColumnIndex()){
							case 0:
								d.setCodigoDisciplina(extrairValorCelula(celula));
								break;
							case 1:
								d.setNome(extrairValorCelula(celula));
								break;
							default:
								throw new Exception("Quantidade de colunas Invalidas");
						}
					}
					disciplinas.add(d);
				}
				workbook.close();
			}
			catch(InvalidFormatException iFE){
				throw new NegocioException("Formata��o do arquivo � invalida");
			}
			catch(IllegalStateException iSE){
				throw new NegocioException("Formata��o incorreta na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
			}
			catch(NumberFormatException nFE){
				throw new NegocioException("Erro ao tentar converter numero na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
			}
			catch(Exception ex){
				throw new NegocioException("Erro na linha " + ++linhaAtual + " coluna " + ++colunaAtual + ": " + ex.getMessage());
			}
			salvarDisciplinas(disciplinas);
		}
		catch(Exception ex){
			
		}
	}

	private void salvarDisciplinas(List<Disciplina> disciplinas) throws NegocioException {
		try{
			DisciplinaDAO dao = new DisciplinaDAO();
			for(Disciplina d: disciplinas)
			{
				if(dao.consultarPorId(d.getCodigoDisciplina()) != null)
					throw new NegocioException("Disciplina " + d.getCodigoDisciplina() + " j� esta cadastrada");
				dao.adicionar(d);
			}
		}
		catch(Exception nEx){
			throw new NegocioException(nEx.getMessage());
		}
	}

	private void validarArquivoETipo(UploadedFile file, Integer tipo) throws NegocioException{
		if(tipo == null)
			throw new NegocioException("Escolha um tipo");
		if(file == null)
			throw new NegocioException("Escolha um Arquivo");
		if(!isXLSOrXLSX(file.getFileName()))
			throw new NegocioException("Tipo do Arquivo Diferente de XLS ou XLSX");
		
	}
	
	private boolean isXLSOrXLSX(String fileName) {
		if(fileName.contains(".")){
			String extensao = fileName.substring(fileName.lastIndexOf("."));
			if(extensao.equals(".xls") || extensao.equals(".xlsx"))
				return true;
		}
		return false;
	}

	private void importarPessoas(UploadedFile file) throws NegocioException{
		List<Pessoa> alunos = new ArrayList<Pessoa>();
		List<Pessoa> professores = new ArrayList<Pessoa>();
		int colunaAtual= 0;
		int linhaAtual = 0;
		try{
			Workbook workbook = WorkbookFactory.create(file.getInputstream());
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowInterator = sheet.rowIterator();
			if(rowInterator.hasNext())
				rowInterator.next();
			while(rowInterator.hasNext()){
				Row row = rowInterator.next();
				Iterator<Cell> cellIterator = row.iterator();
				Pessoa p= new Pessoa();
				if(row.getLastCellNum() != 5)
					throw new NegocioException("Quantidade de colunas invalidas");
				while(cellIterator.hasNext()){
					Cell celula = cellIterator.next();
					linhaAtual = celula.getRowIndex();
					colunaAtual = celula.getColumnIndex();
					switch(celula.getColumnIndex()){
						case 0:
							p.setNome(extrairValorCelula(celula));
							break;
						case 1:
							p.setEmail(extrairValorCelula(celula));
							break;
						case 2:
							p.setDataNasc(celula.getDateCellValue());
							break;
						case 3:
							String sexo = extrairValorCelula(celula);
							if(!"M".equals(sexo)  && !"F".equals(sexo))
								throw new Exception("Sexo diferente de 'F' ou 'M'");
							p.setSexo(sexo);
							break;
						case 4:
							int tipo = Integer.parseInt(extrairValorCelula(celula));
							if(tipo == 1)
								alunos.add(p);
							else if(tipo == 2)
								professores.add(p);
							else throw new Exception(tipo +" n�o � um tipo valido");
							break;
						default:
							throw new Exception("Quantidade de colunas Invalidas");
					}
				}
			}
			workbook.close();	
		}
		catch(InvalidFormatException iFE){
			throw new NegocioException("Formata��o do arquivo � invalida");
		}
		catch(IllegalStateException iSE){
			throw new NegocioException("Formata��o incorreta na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(NumberFormatException nFE){
			throw new NegocioException("Erro ao tentar converter numero na linha " + ++linhaAtual + " coluna " + ++colunaAtual);
		}
		catch(Exception ex){
			throw new NegocioException("Erro na linha " + ++linhaAtual + " coluna " + ++colunaAtual + ": " + ex.getMessage());
		}
		
		salvarAlunos(alunos);
		salvarProfessores(professores);
	}
	
	
	private void salvarProfessores(List<Pessoa> professores) throws NegocioException{
		try{
			if(!professores.isEmpty()){
				ProfessorDAO dao = new ProfessorDAO();
				List<Long> rasDisponiveis = dao.primeirosRADisponivel(professores.size());
				for(int i=0; i < professores.size(); i++){
					Pessoa p = professores.get(i);
					Long ra = rasDisponiveis.get(i);
					p.setSenha(gerarSenha(false));
					p.setRA(ra);
				}
				dao.adicionar(converterPessoasEmProfessores(professores));
			}
		}
		catch(Exception ex){
			throw new NegocioException("problema na grava��o dos professores");
		}
		
	}

	private void salvarAlunos(List<Pessoa> alunos) throws NegocioException {
		try{
			if(!alunos.isEmpty())
			{
				AlunoDAO dao = new AlunoDAO();
				List<Long> rasDisponiveis = dao.primeirosRADisponivel(alunos.size());
				for(int i=0; i < alunos.size(); i++){
					Pessoa p = alunos.get(i);
					Long ra = rasDisponiveis.get(i);
					p.setSenha(gerarSenha(false));
					p.setRA(ra);
				}
				dao.adicionar(converterPessoasEmAlunos(alunos));
			}
		}
		catch(Exception ex){
			throw new NegocioException("problema na grava��o dos alunos");
		}
	}

	private List<Aluno> converterPessoasEmAlunos(List<Pessoa> pessoas){
		List<Aluno> alunos = new ArrayList<Aluno>();
		for(Pessoa p : pessoas){
			Aluno a = new Aluno();
			a.setDataNasc(p.getDataNasc());
			a.setNome(p.getNome());
			a.setEmail(p.getEmail());
			a.setSenha(p.getSenha());
			a.setSexo(p.getSexo());
			a.setRA(p.getRA());
			a.setLogado(p.getLogado());
			alunos.add(a);
		}
		return alunos;
	}
	
	private List<Professor> converterPessoasEmProfessores(List<Pessoa> pessoas){
		List<Professor> professores = new ArrayList<Professor>();
		for(Pessoa p : pessoas){
			Professor pro = new Professor();
			pro.setDataNasc(p.getDataNasc());
			pro.setNome(p.getNome());
			pro.setEmail(p.getEmail());
			pro.setSenha(p.getSenha());
			pro.setSexo(p.getSexo());
			pro.setRA(p.getRA());
			pro.setLogado(p.getLogado());
			professores.add(pro);
		}
		return professores;
	}
	
	private String gerarSenha(boolean easy){
		if(easy)
			return "1";
		
		String[] caracteres = new String[8];
		for(int i=0; i<caracteres.length;i++){
			caracteres[i] = ((char)(((int)(Math.random() * 26)) + 97)) + "";
		}
		String senha = "";
		for(int i=0; i<caracteres.length;i++){
			senha = senha + caracteres[i];
		}
		return senha;
	}
	
	private static String extrairValorCelula(Cell celula){
		if(celula==null)
			return null;
		
		switch (celula.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			return "";
		case Cell.CELL_TYPE_BOOLEAN:
			return celula.getBooleanCellValue()+"";
		case Cell.CELL_TYPE_ERROR:
			return celula.getErrorCellValue()+"";
		case Cell.CELL_TYPE_FORMULA:
			return celula.getCellFormula();
		case Cell.CELL_TYPE_NUMERIC:
			return  ((int)celula.getNumericCellValue()) + "";
		case Cell.CELL_TYPE_STRING:
			return celula.getStringCellValue();
		default:
			return null;
		}
	}

}
