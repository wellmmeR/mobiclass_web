package web.src.controller;

import java.util.List;

import web.src.entity.NegocioException;
import jpa.DAO.DisciplinaDAO;
import jpa.academico.Disciplina;

public class GestaoDisciplinaController extends Controller{

	private static final long serialVersionUID = -6636286454209404677L;

	public List<Disciplina> buscarDisciplinas(String nomeDisciplina, String codDisciplina) throws NegocioException{
		try{
			DisciplinaDAO dDao = new DisciplinaDAO();
			return dDao.buscarDisciplinas(new Disciplina(codDisciplina, nomeDisciplina));
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public void deletarDisciplinas(List<Disciplina> disciplinas) throws NegocioException{
		try{
			DisciplinaDAO dDao = new DisciplinaDAO();
			dDao.deletarRelacionamentos(disciplinas);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
}
