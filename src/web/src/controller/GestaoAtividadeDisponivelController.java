package web.src.controller;

import java.util.List;

import jpa.DAO.AreaAtividadeDAO;
import jpa.DAO.DisciplinaDAO;
import jpa.DAO.TurmaDAO;
import jpa.DAO.relacionamento.AtividadeDisponivelDAO;
import jpa.DAO.relacionamento.TurmaDisciplinaDAO;
import jpa.academico.Disciplina;
import jpa.academico.Turma;
import jpa.academico.TurmaID;
import jpa.atividade.AreaAtividade;
import jpa.atividade.AtividadeDisponivel;
import web.src.entity.NegocioException;

public class GestaoAtividadeDisponivelController extends Controller{

	private static final long serialVersionUID = -349111312579915338L;
	
	public List<AtividadeDisponivel> buscarAtividades(AtividadeDisponivel a) throws Exception{
		AtividadeDisponivelDAO dao = new AtividadeDisponivelDAO();
		return dao.buscarAtividadesDisponiveis(a);
	}
	
	public List<Turma> buscarTodasTurmas(){
		TurmaDAO dao = new TurmaDAO();
		List<Turma> lista = dao.getItensDaTabela();
		return lista;
	}

	public List<Disciplina> buscarTodasDisciplinas(){
		DisciplinaDAO dao = new DisciplinaDAO();
		List<Disciplina> lista = dao.getItensDaTabela();
		return lista;
	}

	public List<AreaAtividade> buscarTodasAreasAtividades(){
		AreaAtividadeDAO dao = new AreaAtividadeDAO();
		List<AreaAtividade> lista = dao.getItensDaTabela();
		return lista;
	}
	
	public List<Disciplina> disciplinasDaTurma(TurmaID id) throws NegocioException{
		try{
			TurmaDisciplinaDAO dao = new TurmaDisciplinaDAO();
			return dao.buscarDisciplinasDaTurma(null, id.getCodigoTurma(), id.getAnoTurma());
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
		
	}
	
	public void deletarAtividadeDisponivel(List<AtividadeDisponivel> atividades) throws NegocioException{
		try{
			AtividadeDisponivelDAO dao = new AtividadeDisponivelDAO();
			dao.deletarPorId(atividades);
		}
		catch(Exception ex){
			throw new NegocioException(ex.getMessage());
		}
		
	}

}
