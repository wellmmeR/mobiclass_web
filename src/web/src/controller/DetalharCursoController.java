package web.src.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import web.src.entity.NegocioException;
import jpa.DAO.TurmaDAO;
import jpa.DAO.relacionamento.AlunoCursoDAO;
import jpa.academico.AlunoCurso;
import jpa.academico.AlunoCursoID;
import jpa.academico.Curso;
import jpa.academico.Turma;
import jpa.pessoa.Aluno;
import jpa.util.Desconsiderar;

public class DetalharCursoController extends Controller{
	
	private static final long serialVersionUID = 93368741841774433L;
	
	public List<Integer> buscarAnoDeTurmas(Curso c) throws NegocioException{
		try{
			TurmaDAO dao = new TurmaDAO();
			return dao.anoDasTurmasDoCurso(c);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public List<Turma> buscarTurmasDoCurso(String codigoCurso, String nomeTurma, String codTurma, Integer anoTurma)throws NegocioException{
		try{
			TurmaDAO dao = new TurmaDAO();
			Map<String, Object> atributos = new HashMap<String, Object>();
			Map<String, Object> subMapa = new HashMap<String, Object>();
			if(anoTurma != null && !anoTurma.equals(0))
				subMapa.put("anoTurma", anoTurma);
			if(codTurma!=null && !codTurma.isEmpty())
				subMapa.put("codigoTurma", codTurma);
			if(nomeTurma!=null && !nomeTurma.isEmpty())
				atributos.put("nomeTurma", nomeTurma);
			
			atributos.put("codCurso", codigoCurso);
			atributos.put("id", subMapa);
			return dao.buscarPorAtributos(atributos);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public List<Aluno> buscarAlunosDoCurso(Curso c, Long raAluno, String nomeAluno, String emailAluno) throws NegocioException{
		try{
			if(raAluno==null)
				raAluno = Desconsiderar.tipoLong;
			
			Aluno a = new Aluno(raAluno, nomeAluno, "", emailAluno, null, null);
			AlunoCursoDAO dao = new AlunoCursoDAO();
			List<Aluno> alunos = dao.buscarAlunosPorCurso(c.getCodCurso(), a);
			return alunos;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw new NegocioException(ex);
		}
	}
	
	public void gravarAlunosDaPesquisa(List<Aluno> alunos, String codCurso) throws NegocioException{
		try{
			if(alunos != null && codCurso != null)
			{
				AlunoCursoDAO acDao = new AlunoCursoDAO();
				Calendar c = Calendar.getInstance();
				List<AlunoCurso> alunosDoCurso = new ArrayList<AlunoCurso>();
				for(Aluno aluno: alunos)
					alunosDoCurso.add(new AlunoCurso(codCurso,c.get(Calendar.YEAR), aluno.getRA()));
				acDao.adicionar(alunosDoCurso);
				alunos = null;
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	public void desvincularAlunoDoCurso(Long ra, String codCurso) throws NegocioException{
		try{
			AlunoCursoDAO acDao = new AlunoCursoDAO();
			acDao.deletarPorId(new AlunoCursoID(codCurso, ra));
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}

}
