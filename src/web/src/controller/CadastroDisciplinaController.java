package web.src.controller;

import jpa.DAO.DisciplinaDAO;
import jpa.academico.Disciplina;
import web.src.entity.NegocioException;

public class CadastroDisciplinaController extends Controller{

	private static final long serialVersionUID = 1L;
	
private String mensagem;
	
	public String gravarDisciplina(String cod,String nome) throws NegocioException{
		try{
			Disciplina d = new Disciplina(cod, nome);
			DisciplinaDAO dDao = new DisciplinaDAO();
			if(camposObrigatorioPreenchido(d) && !existeId(d.getCodigoDisciplina())){
				
				dDao.adicionar(d);
				return "Disciplina " + d.getCodigoDisciplina() + " criada com sucesso";
			}
			else{
				throw new NegocioException(mensagem);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
			
	}
	
	public String atualizarDisciplina(String cod,String nome) throws NegocioException{
		try{
			Disciplina d = new Disciplina(cod, nome);
			DisciplinaDAO dDao = new DisciplinaDAO();
			if(camposObrigatorioPreenchido(d)){
				dDao.atualizar(d);
				return "Disciplina " + d.getCodigoDisciplina() + " atualizada com sucesso";
			}
			else{
				throw new NegocioException(mensagem);
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	private boolean camposObrigatorioPreenchido(Disciplina d){
		mensagem =  "Preencher Campos Obrigatorios";
		if(d == null)
			return false;
		if(d.getCodigoDisciplina() == null || d.getCodigoDisciplina().trim().isEmpty())
			return false;
		if(d.getNome() == null && d.getNome().trim().isEmpty())
			return false;
		
		return true;
	}
	
	private boolean existeId(String codDisciplina) throws NegocioException{
		try{
			DisciplinaDAO dDao = new DisciplinaDAO();
			Disciplina d = dDao.consultarPorId(codDisciplina);
			if(d == null)
				return false;
			else{
				mensagem =  "Codigo em uso";
				return true;
			}
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}

}
