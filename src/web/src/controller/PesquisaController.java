package web.src.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jpa.DAO.CursoDAO;
import jpa.DAO.TurmaDAO;
import jpa.DAO.relacionamento.AlunoCursoDAO;
import jpa.DAO.relacionamento.AlunoTurmaDAO;
import jpa.academico.Curso;
import jpa.academico.Turma;
import jpa.pessoa.Aluno;
import jpa.pessoa.Pessoa;
import jpa.pessoa.Professor;
import jpa.util.Desconsiderar;
import web.src.entity.NegocioException;
import web.src.entity.Util;

public class PesquisaController extends Controller{

	private static final long serialVersionUID = 2094508574147571236L;
	
	public List<Pessoa> procurarPessoasSemTurma(Long raPessoa, String nomePessoa, String emailPessoa, Date dtNascimento, String sexo, boolean isProfessor) throws NegocioException{
		try{
			Long ra = raPessoa == null? Desconsiderar.tipoLong: raPessoa.longValue();
			Pessoa p = new Pessoa(ra, nomePessoa, null, emailPessoa, dtNascimento, sexo);
			List<Pessoa> lista = new ArrayList<Pessoa>();
			if(isProfessor){
				lista = converterProfessores(procurarProfessor());
			}
			else{
				lista = converterAlunos(procurarAluno(p));
			}
			return lista;
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			throw new NegocioException(ex);
		}
	}
	
	private List<Pessoa> converterAlunos(List<Aluno> alunos){
		List<Pessoa> novaLista = new ArrayList<>();
		for(Aluno a : alunos)
			novaLista.add(a);
		return novaLista;
	}
	private List<Pessoa> converterProfessores(List<Professor> professores){
		List<Pessoa> novaLista = new ArrayList<>();
		for(Professor a : professores)
			novaLista.add(a);
		return novaLista;
	}
	
	private List<Professor> procurarProfessor(){
		//TODO DFL;
		return null;
	}
	private List<Aluno> procurarAluno(Pessoa p) throws NegocioException{
		try{
			AlunoCursoDAO dao = new AlunoCursoDAO();
			return dao.alunosSemCurso(p);
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<Curso> buscarCursosNaoMatriculadosPeloAluno(Long ra, String nome, String codCurso, Integer disponivel) throws NegocioException{
		try{
			CursoDAO dao = new CursoDAO();
			AlunoCursoDAO acDao = new AlunoCursoDAO();
			List<Curso> cursosDoAluno = acDao.buscarCursosPorAluno(new Curso(codCurso, nome,disponivel), ra);
			List<Curso> cursos = dao.buscarCursos(codCurso, nome, disponivel);
			Util.removerListaDentroDeLista(cursos, cursosDoAluno);
			return cursos;
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<Turma> buscarTurmasNaoMatriculadasPeloAluno(Long ra, String nome, String codTurma, Integer anoTurma, String codCurso) throws NegocioException{
		try{
			if(anoTurma == null)
				anoTurma = Desconsiderar.tipoInt;
			AlunoTurmaDAO atDao = new AlunoTurmaDAO();
			TurmaDAO tDao = new TurmaDAO();
			Turma t = new Turma(codTurma, nome, anoTurma, codCurso);
			List<Turma> turmasDoAluno = atDao.consultarTurmasDoAluno(ra, t);
			List<Turma> turmas = tDao.consultarTurmas(t);
			Util.removerListaDentroDeLista(turmas, turmasDoAluno);
			return turmas;
		}
		catch(Exception ex){
			throw new NegocioException(ex);
		}
	}

}
