package web.src.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jpa.pessoa.Pessoa;
import web.src.entity.SessaoEnum;

public class Filtro implements Filter{

	@Override
	public void destroy() {
		
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
			
		Pessoa usuarioLogado =(Pessoa) ((HttpServletRequest) arg0).getSession().getAttribute(SessaoEnum.KEY_USUARIO.name());
		if(usuarioLogado == null)
		{
			String contextPath = ((HttpServletRequest) arg0).getContextPath();
			((HttpServletResponse) arg1).sendRedirect(contextPath + "/login.xhtml");
		} 
		else
			arg2.doFilter(arg0, arg1);
	}

}
