package web.src.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import web.src.controller.GestaoDisciplinaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;
import jpa.academico.Disciplina;

@ManagedBean
@ViewScoped
public class GestaoDisciplinaView extends View{

	private static final long serialVersionUID = 2605186228949879947L;
	private static final String NOME_PAGINA = "gestao-disciplina.xhtml";
	
	private String nomeDisciplina;
	private String codDisciplina;
	private List<Disciplina> disciplinas;
	private List<Disciplina> disciplinasSelecionadas;
	private GestaoDisciplinaController controller;
	
	public GestaoDisciplinaView() {
		controller = new GestaoDisciplinaController();
	}
	@PostConstruct
	public void init(){
		pesquisar();
	}
	
	public void pesquisar(){
		try{
			disciplinas = controller.buscarDisciplinas(getNomeDisciplina(), getCodDisciplina());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void detalharDisciplina(){
		
	}
	
	public void modificarDisciplina(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_DISCIPLINA, disciplinasSelecionadas.get(0));
		Util.irPara("cadastro-disciplina.xhtml", NOME_PAGINA);
	}
	
	public void cadastrarDisciplina(){
		Util.irPara("cadastro-disciplina.xhtml", NOME_PAGINA);
	}
	
	public void deletarDisciplina(){
		try{
			controller.deletarDisciplinas(disciplinasSelecionadas);
			Util.removerListaDentroDeLista(disciplinas, disciplinasSelecionadas);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public boolean nenhumaDisciplinaSelecionada(){
		if(disciplinasSelecionadas == null || disciplinasSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public boolean umaDisciplinaSelecionada(){
		if(nenhumaDisciplinaSelecionada() || disciplinasSelecionadas.size() != 1)
			return false;
		return true;
	}

	public String getNomeDisciplina() {
		return nomeDisciplina;
	}

	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public List<Disciplina> getDisciplinasSelecionadas() {
		return disciplinasSelecionadas;
	}

	public void setDisciplinasSelecionadas(List<Disciplina> disciplinasSelecionadas) {
		this.disciplinasSelecionadas = disciplinasSelecionadas;
	}
	
	

}
