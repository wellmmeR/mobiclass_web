package web.src.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import web.src.controller.LoginController;
import web.src.entity.NegocioException;
import web.src.entity.Sessao;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class LoginView extends View{

	private static final long serialVersionUID = 1L;
	private String usuario;
	private String senha;
	
	private LoginController controller;
	
	@ManagedProperty(value="#{sessao}")
	private Sessao sessao;
	
	public Sessao getSessao() {
		return sessao;
	}

	public void setSessao(Sessao sessao) {
		this.sessao = sessao;
	}

	public LoginView(){
		
		controller = new LoginController();
	}
	
	public void logar(){
		try
		{
			controller.tentarLogar(getUsuario(), getSenha(), getSessao());
		}
		catch(NegocioException nEx)
		{
			Util.exibirMensagemDeErro("Erro ao tentar logar:", nEx.getMessage());
		}
		
	}
	
	public void logout(){
		controller.logout();
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}
