package web.src.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import web.src.controller.CadastroAreaAtividadeController;
import web.src.entity.NegocioException;
import web.src.entity.Util;
@ManagedBean
@ViewScoped
public class CadastroAreaAtividadeView extends View{

	private static final long serialVersionUID = 1L;
	
	private String nome;
	private CadastroAreaAtividadeController controller;
	
	public CadastroAreaAtividadeView() {
		controller = new CadastroAreaAtividadeController();
	}
	
	public void cadastrar(){
		try{
			String mensagem = controller.gravarAreaAtividade(getNome());
			Util.exibirMensagemDeSucesso(null, mensagem);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void limpar(){
		nome = "";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
