package web.src.view;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.pessoa.Aluno;
import web.src.controller.GestaoPessoaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class GestaoAlunoView extends View{

	private static final long serialVersionUID = 4145875721469706954L;
	private static final String NOME_PAGINA = "gestao-aluno.xhtml";
	private static final int ALUNO = 1;
	
	private Long raALuno;
	private String nomeAluno;
	private String emailAluno;
	private Date dtNascimentoAluno;
	private String sexoAluno;
	
	private List<Aluno> alunos;
	private List<Aluno> alunosSelecionados;
	private GestaoPessoaController controller;
	
	public GestaoAlunoView() {
		controller = new GestaoPessoaController();
	}
	@PostConstruct
	public void init(){
		pesquisar();
	}
	
	public void pesquisar(){
		try{
			alunos = controller.procurarAlunos(getRaALuno(), getNomeAluno(), getEmailAluno(), getDtNascimentoAluno(), getSexoAluno());
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void cadastrarAluno(){
		redirecionarPaginaCadastro();
	}
	
	public void detalharAluno(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_ALUNO, alunosSelecionados.get(0));
		Util.irPara("detalhar-aluno.xhtml", NOME_PAGINA);
	}
	
	public void modificarAluno(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_PESSOA, alunosSelecionados.get(0));
		redirecionarPaginaCadastro();
	}
	
	public void deletarAluno(){
		try{
			controller.deletarAlunos(alunosSelecionados);
			Util.removerListaDentroDeLista(alunos, alunosSelecionados);
			String plural = alunosSelecionados.size()==1 ? "":"s"; 
			Util.exibirMensagemDeSucesso(null, "Aluno"+plural + " Deletado"+plural + " com sucesso");
			alunosSelecionados = null;
		}
		catch(Exception ex){
			Util.exibirMensagemDeErro(null, "Ocorreu um problema ao excluir o aluno");
			ex.printStackTrace();
		}
		
	}
	
	private void redirecionarPaginaCadastro(){
		Util.adicionarObjetoNaSessao(SessaoEnum.TIPO_PESSOA, ALUNO);
		Util.irPara("cadastro-pessoa.xhtml", NOME_PAGINA);
	}
	
	public String infoAlunosSelecionados(){
		String mensagem = null;
		if(alunosSelecionados != null)
		{
			if(alunosSelecionados.size() == 1)
				mensagem = "o Aluno " + alunosSelecionados.get(0).getNome() + "?";
			else
			{
				int size = alunosSelecionados.size();
				mensagem = "os " + size +" Alunos selecionados?";
			}
		}
			 
		return mensagem;
	}
	
	public boolean temAlunoSelecionado(){
		if(alunosSelecionados != null && !alunosSelecionados.isEmpty())
			return true;
		return false;
	}
	
	public boolean temUmAlunoSelecionado(){
		if(temAlunoSelecionado() && alunosSelecionados.size() == 1)
			return true;
		return false;
	}
	
	
	public Long getRaALuno() {
		return raALuno;
	}
	public void setRaALuno(Long raALuno) {
		if(raALuno != null && raALuno.equals(0L))
			this.raALuno = null;
		else
			this.raALuno = raALuno;
	}
	public String getNomeAluno() {
		return nomeAluno;
	}
	public void setNomeAluno(String nomeAluno) {
		this.nomeAluno = nomeAluno;
	}
	public String getEmailAluno() {
		return emailAluno;
	}
	public void setEmailAluno(String emailAluno) {
		this.emailAluno = emailAluno;
	}
	public Date getDtNascimentoAluno() {
		return dtNascimentoAluno;
	}
	public void setDtNascimentoAluno(Date dtNascimentoAluno) {
		this.dtNascimentoAluno = dtNascimentoAluno;
	}
	public String getSexoAluno() {
		return sexoAluno;
	}
	public void setSexoAluno(String sexoAluno) {
		this.sexoAluno = sexoAluno;
	}
	public List<Aluno> getAlunos() {
		return alunos;
	}
	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}
	public List<Aluno> getAlunosSelecionados() {
		return alunosSelecionados;
	}
	public void setAlunosSelecionados(List<Aluno> alunosSelecionados) {
		this.alunosSelecionados = alunosSelecionados;
	}
	

}
