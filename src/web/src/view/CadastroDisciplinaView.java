package web.src.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Disciplina;
import web.src.controller.CadastroDisciplinaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class CadastroDisciplinaView extends View{

	private static final long serialVersionUID = 1L;
	private String cod;
	private String nome;
	private Disciplina disciplina;
	private boolean atualizando;
	
	CadastroDisciplinaController controller;
	public CadastroDisciplinaView() {
		controller = new CadastroDisciplinaController();
	}
	
	@PostConstruct
	public void init(){
		recuperarObjetosDaSessao();
		if(disciplina != null)
		{
			cod = disciplina.getCodigoDisciplina();
			nome = disciplina.getNome();
			setAtualizando(true);
		}
			
	}
	
	public void recuperarObjetosDaSessao(){
		disciplina = (Disciplina)Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_DISCIPLINA);
	}
	
	public void cadastrar(){
		try{
			String mensagem = controller.gravarDisciplina(getCod(), getNome());
			Util.exibirMensagemDeSucesso(null, mensagem);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
		
	}
	
	public void atualizar(){
		try{
			String mensagem = controller.atualizarDisciplina(getCod(), getNome());
			Util.exibirMensagemDeSucesso(null, mensagem);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro(nEx.getTitulo(), nEx.getMessage());
		}
	}
	
	public void voltar(){
		Util.irPaginaAnterior();
	}
	
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtualizando() {
		return atualizando;
	}

	public void setAtualizando(boolean atualizando) {
		this.atualizando = atualizando;
	}

}
