package web.src.view;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Curso;
import jpa.academico.Turma;
import jpa.util.Desconsiderar;
import web.src.controller.GestaoTurmaController;
import web.src.entity.NegocioException;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class GestaoTurmaView extends View{
	
	private static final long serialVersionUID = -5515626985866785532L;
	private static final String NOME_PAGINA = "gestao-turma.xhtml";
	
	private String nomeTurma;
	private String codTurma;
	private String codCurso;
	private List<Curso> cursos;
	private Integer anoTurma;
	private List<Integer> anosDasTurmas;
	
	private List<Turma> turmas;
	private List<Turma> turmasSelecionadas;
	private boolean viewIniciada;
	
	private GestaoTurmaController controller;
	
	public void init(){
		if(!viewIniciada){
			controller = new GestaoTurmaController();
			pesquisarCursos();
			pesquisarAnosDeTurmas();
			anoTurma = Desconsiderar.tipoInt;
			pesquisar();
			viewIniciada = true;
		}
	}
	
	private void pesquisarAnosDeTurmas() {
		anosDasTurmas = controller.todosAnosDeTurmas();
	}

	private void pesquisarCursos() {
		cursos = controller.buscarTodosCursos();
	}

	public void pesquisar(){
		try{
			turmas = controller.buscarTurmas(nomeTurma, codCurso, codTurma, anoTurma);
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro na pesquisa", nEx.getMessage());
		}
	}
	
	public void cadastrar(){
		Util.irPara("cadastro-turma.xhtml", NOME_PAGINA);
	}
	
	public void modificar(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_TURMA, turmasSelecionadas.get(0));
		Util.irPara("cadastro-turma.xhtml", NOME_PAGINA);
	}
	
	public void deletar(){
		try{
			controller.deletarTurmas(turmasSelecionadas);
			Util.removerListaDentroDeLista(turmas, turmasSelecionadas);
			turmasSelecionadas = null;
		}
		catch(NegocioException nEx){
			Util.exibirMensagemDeErro("Erro na pesquisa", nEx.getMessage());
		}
	}
	
	public void detalhar(){
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_TURMA, turmasSelecionadas.get(0));
		Util.irPara("detalhar-turma.xhtml", NOME_PAGINA);
	}
	
	public boolean temTurmaSelecionada(){
		if(turmasSelecionadas != null && !turmasSelecionadas.isEmpty())
			return true;
		return false;
	}
	
	public boolean temUmaTurmaSelecionada(){
		if(temTurmaSelecionada() && turmasSelecionadas.size() == 1)
			return true;
		return false;
	}

	public String getNomeTurma() {
		return nomeTurma;
	}

	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}

	public String getCodTurma() {
		return codTurma;
	}

	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public Integer getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}

	public List<Integer> getAnosDasTurmas() {
		return anosDasTurmas;
	}

	public void setAnosDasTurmas(List<Integer> anosDasTurmas) {
		this.anosDasTurmas = anosDasTurmas;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public List<Turma> getTurmasSelecionadas() {
		return turmasSelecionadas;
	}

	public void setTurmasSelecionadas(List<Turma> turmasSelecionadas) {
		this.turmasSelecionadas = turmasSelecionadas;
	}

}
