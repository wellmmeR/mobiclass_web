package web.src.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.atividade.Atividade;

import org.primefaces.context.RequestContext;

import web.src.controller.CadastroJogoForcaController;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class CadastroJogoForcaView extends View{

	private static final long serialVersionUID = 9018367294535453354L;
	private static final String NOME_PAGINA = "cadastro-jogo-forca";
	
	private CadastroJogoForcaController controller;
	private String perguntaForca;
	private String palavra;
	private String dica;
	private Integer numTentativas;
	private int disponivel;
	private Atividade atividade;
	private boolean iniciado;
	
	public CadastroJogoForcaView() {
		controller = new CadastroJogoForcaController();
	}
	
	public void init(){
		try{
			if(!iniciado){
				pegarObjetosDaSessao();
				iniciado = true;
			}
		}
		catch(Exception ex){
			Util.exibirMensagemDeErro("Erro na inicialização", ex.getMessage());
		}
	}
	
	private void pegarObjetosDaSessao() {
		atividade = (Atividade) Util.pegarObjetoDaSessaoERemover(SessaoEnum.OBJETO_ATIVIDADE);
		if(atividade == null)
			redirecionarGestaoAtividade();
	}
	
	public void cadastrarJogoForca(){
		try{
			String mensagem = controller.cadastrarJogoForca(getAtividade().getIdAtividade().getCodigoAtividade(), getAtividade().getIdAtividade().getCodigoDisciplina(), 
					getAtividade().getIdAtividade().getAreaAtividade(), disponivel, perguntaForca, palavra, dica, numTentativas);
			Util.exibirMensagemDeSucesso(null, mensagem);
			RequestContext.getCurrentInstance().execute("PF('confirmarNovoJogoVar').show();");
		}
		catch(Exception ex){
			Util.exibirMensagemDeErro("Erro no cadastro do jogo", ex.getMessage());
		}
	}
	
	public void limparCampos(){
		perguntaForca = "";
		palavra = "";
		dica = "";
		numTentativas = null;
		disponivel = 0;
	}
	
	public void redirecionarGestaoAtividade(){
		Util.irPara("gestao-atividade.xhtml", NOME_PAGINA);
	}

	public CadastroJogoForcaController getController() {
		return controller;
	}

	public void setController(CadastroJogoForcaController controller) {
		this.controller = controller;
	}

	public String getPerguntaForca() {
		return perguntaForca;
	}

	public void setPerguntaForca(String perguntaForca) {
		this.perguntaForca = perguntaForca;
	}

	public String getPalavra() {
		return palavra;
	}

	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}

	public String getDica() {
		return dica;
	}

	public void setDica(String dica) {
		this.dica = dica;
	}

	public Integer getNumTentativas() {
		return numTentativas;
	}

	public void setNumTentativas(Integer numTentativas) {
		this.numTentativas = numTentativas;
	}

	public int getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(int disponivel) {
		this.disponivel = disponivel;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
}
