package web.src.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.academico.Disciplina;
import jpa.atividade.AreaAtividade;
import jpa.atividade.Atividade;
import jpa.util.Desconsiderar;
import web.src.controller.GestaoAtividadeController;
import web.src.entity.SessaoEnum;
import web.src.entity.Util;

@ManagedBean
@ViewScoped
public class GestaoAtividadeView extends View {

	private static final long serialVersionUID = 5860418394259525141L;
	private static final String NOME_PAGINA = "gestao-atividade.xhtml";

	private String codAtividade;
	private String codDisciplina;
	private List<Disciplina> todasAsDisciplinas;
	private Long areaAtividade;
	private List<AreaAtividade> todasAsAreasDeAtividades;
	private int disponivel;
	private String nomeAtividade;

	private List<Atividade> atividades;
	private List<Atividade> atividadesSelecionadas;

	private GestaoAtividadeController controller;

	public GestaoAtividadeView() {
		controller = new GestaoAtividadeController();
	}

	@PostConstruct
	public void init() {
		pesquisarDisciplinas();
		pesquisarAreaAtividade();
		disponivel = Desconsiderar.tipoInt;
		areaAtividade = Desconsiderar.tipoLong;
		pesquisar();
	}

	private void pesquisarAreaAtividade() {
		todasAsAreasDeAtividades = controller.buscarTodasAreasAtividades();

	}

	private void pesquisarDisciplinas() {
		todasAsDisciplinas = controller.buscarTodasDisciplinas();

	}

	public void pesquisar() {
		try {
			atividades = controller.buscarAtividades(
					new Atividade(codAtividade, nomeAtividade, codDisciplina, disponivel, areaAtividade));
		} catch (Exception ex) {
			Util.exibirMensagemDeErro("Erro na Pesquisa", ex.getMessage());
		}
	}

	public void cadastrarAtividade() {
		Util.irPara("cadastro-atividade.xhtml", NOME_PAGINA);
	}

	public void deletarAtividade() {
		try {
			controller.deletarAtividade(atividadesSelecionadas);
			String msg = atividadesSelecionadas.size() > 1 ? "Atividades Deletadas" : "Atividade Deletada";
			Util.removerListaDentroDeLista(atividades, atividadesSelecionadas);
			atividadesSelecionadas = new ArrayList<Atividade>();
			Util.exibirMensagemDeSucesso(null, msg);
		} catch (Exception ex) {
			Util.exibirMensagemDeErro("Erro ao deletar Atividade: ", ex.getMessage());
		}
	}

	public void detalharAtividade() {

	}

	public void modificarAtividade() {
		Util.adicionarObjetoNaSessao(SessaoEnum.OBJETO_ATIVIDADE, atividadesSelecionadas.get(0));
		Util.irPara("cadastro-atividade.xhtml", NOME_PAGINA);
	}

	public boolean temAtividadeSelecionada() {
		if (atividadesSelecionadas != null && !atividadesSelecionadas.isEmpty())
			return true;
		return false;
	}

	public boolean temUmaAtividadeSelecionada() {
		if (temAtividadeSelecionada() && atividadesSelecionadas.size() == 1)
			return true;
		return false;
	}

	public String intDisponivelToString(Atividade at) {
		int disponivel = at.isDisponivel();
		String mensagem;
		if (disponivel == 1)
			mensagem = "Sim";
		else
			mensagem = "N�o";
		return mensagem;
	}

	public String nomeAreaAtividade(Long id) {
		String nomeAreaAtividade = "";
		if (id != null)
			nomeAreaAtividade = controller.getNomeAreaAtividade(id);
		return nomeAreaAtividade;
	}

	public String getCodAtividade() {
		return codAtividade;
	}

	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}

	public Long getAreaAtividade() {
		return areaAtividade;
	}

	public void setAreaAtividade(Long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

	public int getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(int disponivel) {
		this.disponivel = disponivel;
	}

	public String getNomeAtividade() {
		return nomeAtividade;
	}

	public void setNomeAtividade(String nomeAtividade) {
		this.nomeAtividade = nomeAtividade;
	}

	public List<Disciplina> getTodasAsDisciplinas() {
		return todasAsDisciplinas;
	}

	public void setTodasAsDisciplinas(List<Disciplina> todasAsDisciplinas) {
		this.todasAsDisciplinas = todasAsDisciplinas;
	}

	public List<AreaAtividade> getTodasAsAreasDeAtividades() {
		return todasAsAreasDeAtividades;
	}

	public void setTodasAsAreasDeAtividades(List<AreaAtividade> todasAsAreasDeAtividades) {
		this.todasAsAreasDeAtividades = todasAsAreasDeAtividades;
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public List<Atividade> getAtividadesSelecionadas() {
		return atividadesSelecionadas;
	}

	public void setAtividadesSelecionadas(List<Atividade> atividadesSelecionadas) {
		this.atividadesSelecionadas = atividadesSelecionadas;
	}
}
