package web.src.entity;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jpa.pessoa.Pessoa;

@SessionScoped
@ManagedBean(name="sessao")
public class Sessao implements Serializable{
	
	private static final long serialVersionUID = -5666539276005739561L;
	
	private Pessoa usuario;

	public Pessoa getUsuario() {
		return usuario;
	}

	public void setUsuario(Pessoa usuario) {
		this.usuario = usuario;
	}
	
	public String primeiroNomeUsuario(){
		return usuario.getNome().split(" ")[0];
	}


}
