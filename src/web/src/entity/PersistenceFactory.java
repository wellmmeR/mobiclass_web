package web.src.entity;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
@ManagedBean(eager=true)
@ApplicationScoped
public class PersistenceFactory {

	private EntityManagerFactory factory;
	private static final String PERSISTENCE_UNIT_NAME = "ServerAndroid";
	public static EntityManagerFactory FACTORY = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	
	
	public PersistenceFactory() {
		if(factory == null)
			factory = PersistenceFactory.FACTORY;	
	}

	public EntityManagerFactory getFactory(){
		return factory;
	}
	
}
